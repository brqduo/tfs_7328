export const config = {
  TST: {
    URL_SINTEGRE: "http://tst-git-065.ons.org.br:83/ONS.Sintegre.Proxy",
    URL_SINTEGRE_PROXYS: {
      verificaEmail: "/proxy/cadastro/verificaemail?email=",
      dominioConhecido: "/proxy/cadastro/publicossemdominio",
      aceiteCadastro: "/api/aceite",
      cadastroExterno: "/proxy/cadastro/cadastroexterno",
      listaPerfilPublico: "/proxy/cadastro/publicos?filtro=SemDominio"

    },
    recaptchaKey: '6LdqEqEUAAAAAMU5YkbfddoMlg9R4iQ8sU7gqriE',
    linksExternos: {
      PoliticaPrivacidade: 'http://dsvportalrelacionamento.ons.org.br/sites/cadastro/paginas/condicoes-de-uso.aspx',
      Empresa: 'http://dsvportalrelacionamento.ons.org.br/sites/cadastro/paginas/cadastro-de-empresa.aspx'
    },
    backgroundIMG: 'https://poptst.ons.org.br/ons.pop.federation/Content/img/background.png'
  }
}
