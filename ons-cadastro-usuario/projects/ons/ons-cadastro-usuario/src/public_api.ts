/*
 * Public API Surface of ons-cadastro-usuario
 */

export * from './lib/ons-cadastro-usuario.service';
export * from './lib/ons-cadastro-usuario.component';
export * from './lib/ons-cadastro-usuario.module';
