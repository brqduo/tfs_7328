import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OnsCadastroUsuarioComponent } from './ons-cadastro-usuario.component';

describe('OnsCadastroUsuarioComponent', () => {
  let component: OnsCadastroUsuarioComponent;
  let fixture: ComponentFixture<OnsCadastroUsuarioComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OnsCadastroUsuarioComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OnsCadastroUsuarioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
