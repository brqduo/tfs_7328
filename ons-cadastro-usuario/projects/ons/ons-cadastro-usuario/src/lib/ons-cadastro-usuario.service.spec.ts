import { TestBed, inject } from '@angular/core/testing';

import { OnsCadastroUsuarioService } from './ons-cadastro-usuario.service';

describe('OnsCadastroUsuarioService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [OnsCadastroUsuarioService]
    });
  });

  it('should be created', inject([OnsCadastroUsuarioService], (service: OnsCadastroUsuarioService) => {
    expect(service).toBeTruthy();
  }));
});
