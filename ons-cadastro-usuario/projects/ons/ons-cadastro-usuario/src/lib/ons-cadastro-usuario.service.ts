import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, observable } from 'rxjs';
import { BaseService } from 'projects/ons/mobile-login/src/public_api';
import { config } from '../config';

@Injectable({
  providedIn: 'root'
})
export class OnsCadastroUsuarioService {
  public _permiteNovoUsuario:boolean = false;

  set permiteNovoUsuario(value:boolean) {
    this._permiteNovoUsuario = value
  }

  get permiteNovoUsuario():boolean {
    return this._permiteNovoUsuario;
  }

  constructor(private http: HttpClient,
              private baseSrv: BaseService) { }


    
  salvarCadastroExterno(usuarioObj: any): Observable<any>{
    return this.baseSrv.executePost(config.TST.URL_SINTEGRE + config.TST.URL_SINTEGRE_PROXYS.cadastroExterno,"",usuarioObj);
  }


  aceiteSintegre(aceiteObj:any): Observable<any>{
    return this.baseSrv.executePost(config.TST.URL_SINTEGRE + config.TST.URL_SINTEGRE_PROXYS.aceiteCadastro,"",aceiteObj);
  }

  carregarPerfis(): Observable<any>{

    return this.baseSrv.executeGet(config.TST.URL_SINTEGRE + config.TST.URL_SINTEGRE_PROXYS.listaPerfilPublico,"");
  }

  verificaEmailOns(email: string): Observable<any>{

  return this.baseSrv.executeGet(config.TST.URL_SINTEGRE + config.TST.URL_SINTEGRE_PROXYS.verificaEmail + email,"");

  }

}
