import { NgModule } from '@angular/core';
import { OnsCadastroUsuarioComponent } from './ons-cadastro-usuario.component';

@NgModule({
  imports: [
  ],
  declarations: [OnsCadastroUsuarioComponent],
  exports: [OnsCadastroUsuarioComponent]
})
export class OnsCadastroUsuarioModule { }
