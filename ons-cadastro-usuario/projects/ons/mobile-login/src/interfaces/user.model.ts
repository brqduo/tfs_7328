export interface User {
    User_Name: string;
    User_Full_Name: string;
    Connected_At: Date;
    Connected_Time: number;
    Connected: boolean;
    DeviceProperties: any[];
    email: string;
    Claims: any[];
}
