export interface Device {
    hasFinger: boolean;
    hasNetwork: boolean;
}