export interface RunTimeUser {
    UserName: string;
    UserPass: string;
    Changed: boolean;
    passwordType: string;
    passwordIcon: string;
}
