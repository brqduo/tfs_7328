export interface StatusTable {
    Code: number;
    Name: string;
    Description: string;
}
