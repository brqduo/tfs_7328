export interface Status {
    Code: number;
    Description: string;
    data: Date;
    isError: boolean;
    isWarning: boolean;
}