

import { Component } from '@angular/core';
import { NavController, NavParams, Platform, AlertController, Config } from 'ionic-angular';
import { TokenService } from '../../lib/token.service';
/** SERVICES */

import { LoginService } from '../../lib/login.service';
import { UserService } from '../../lib/user.service';
import { AnalyticsService } from '@ons/ons-mobile-analytics';
import { FingerprintAIO } from '@ionic-native/fingerprint-aio';
import { ErrorService } from '../../lib/error.service';
import { ImageService } from '../../lib/images.service';
import { StorageService } from '../../lib/storage.service';
import { UtilService } from '../../lib/util.service';
import { NetWorkService } from '../../lib/network.service';

/** INTERFACES */
import * as runtimeUser from '../../interfaces/runtimeuser.model';

/** CONFIG */
import { Config as config } from './../../environment/config';
import { CadastroPage } from 'projects/ons/ons-cadastro-usuario/src/Pages/cadastro/cadastro';


@Component({
  selector: 'page-login',
  templateUrl: './login.html',
  styleUrls: ['./login.scss']
})

/**
 * Tela de login
 */
export class LoginPage {
  backgrounds = [];
  vimAquiAntes = 0;
  passwordType = 'password';
  passwordIcon = 'eye-off';
  public usuario: runtimeUser.RunTimeUser = <runtimeUser.RunTimeUser>{
    UserName: 'ONS\\', UserPass: '', Changed: false, passwordType: 'password', passwordIcon: 'eye-off'
  };
  userChanged = false;
  versaoApp: string = "";
  Fundo_Imagem: any;
  /**
   * Contructor da interface
   * Este constructor trata o logon automático além de validar a opção de exit do app
   *
   */
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public loginSrv: LoginService,
    public imgSrv: ImageService,
    public userSrv: UserService,
    public faio: FingerprintAIO,
    public storageSrv: StorageService,
    public errorSrv: ErrorService,
    public utilSrv: UtilService,
    public fingerAuth: FingerprintAIO,
    public analyticsSrv: AnalyticsService,
    public netSrv: NetWorkService,
    public tokenSrv: TokenService,
    public platform: Platform
  ) {
    this.platform.ready().then(() => {   // should i wait for this
      // statusBar.styleDefault();
      console.log('platdform is ready');
      this.netSrv.updateNetworkStatus();
      this.utilSrv.VersaoAtual.subscribe((versao) => {
        this.versaoApp = versao;
      });
      this.utilSrv.isAtual.subscribe((obj: any) =>{
        this.utilSrv.alerta(obj.mensagem,config.versionamento.MSG_TIME,config.versionamento.MSG_POSICAO,config.versionamento.MSG_TITULO,true);
      })
    });
    let tempUser = this.tokenSrv.getUserNameFromToken();
    this.backgrounds = imgSrv.Images;
    this.storageSrv.recuperar(config.Keys.NOMEUSUARIO)
      .then((usuario: string) => {
        if (usuario === undefined || usuario === null) {
          this.usuario.UserName = 'ONS\\';
        } else {
          this.usuario.UserName = usuario;
        }
      });
    if (tempUser === undefined || tempUser === null) {
      this.usuario.UserName = 'ONS\\';
    } else {
      this.usuario.UserName = tempUser;
    }

    this.storageSrv.recuperar(config.Keys.USUARIO_SAIU)
      .then((opcaoAuto: boolean) => {
        let autoLogin = false;
        this.storageSrv.recuperar(config.Keys.FINGER_KEY)
          .then((opcaoFinger: boolean) => {
            if ((!opcaoFinger) && (!opcaoAuto)) {
              autoLogin = true;
            } else {
              if ((!opcaoFinger) && (opcaoAuto)) {
                autoLogin = false;
              } else {
                if ((opcaoFinger) && (opcaoAuto)) {
                  autoLogin = true;
                } else {
                  if ((opcaoFinger) && (!opcaoAuto)) {
                    autoLogin = true;
                  }
                }
              }
            }
            if ((autoLogin) && (!loginSrv.VoltandoSaida)) {
              this.loginAutomatico();
            }
          });
      });
      const img = Config.TST.backgroundIMG

      this.Fundo_Imagem = {
        'background': '#156FAB url(' + img + ')'
      }
  }

  /**
   * Carga inicial da tela de login
   */
  ionViewDidLoad() {
    this.userSrv.onUserChange
      .subscribe((u: any) => {
        console.log('usuario retornado no subscribe ', JSON.stringify(u));
      });
  }

  /**
   * Procedimento de exibição ou não da senha na tela
   */
  hideShowPassword() {
    this.passwordType = this.passwordType === 'text' ? 'password' : 'text';
    this.passwordIcon = this.passwordIcon === 'eye-off' ? 'eye' : 'eye-off';
  }


  /**
   * Indica se o usuário informado na interface de login foi alterado
   * Qualquer edição é tratada como alteração.
   * @param x Evento da tela de login
   */
  usuarioAlterado(x) {
    this.userChanged = true;
    this.loginSrv.Usuario_alterado = this.userChanged;
  }

  goToCadastro() {
this.navCtrl.push(CadastroPage)
  }
  /**
   * Executa o logon automático quando o usuário retorna à aplicação.
   * O token arazenado é utilizado neste processo
   */
  loginAutomatico() {
    //   this.usuarioAlterado().then((alterado) => {
    // if (!this.userChanged) {
    // this.utilSrv.ativarBloqueioMsg(config.mensagemGenerica.ENTRANDO);
    this.userChanged = false;
    this.loginSrv.Usuario_alterado = this.userChanged;
    this.loginSrv.validateTokenFlow().then(data => {
      if (data === null) {
        this.loginSrv.onConectedChange.next(this.userSrv.empty());
        this.errorSrv.add(config.Error.TOKEN_NULL, 'Token nulo on inválido');  // Tratar null
      } else {
        if (this.tokenSrv.isValid(data)) {
          if (!this.tokenSrv.isExpired(data)) {
            this.tokenSrv.setToken(data);
            this.storageSrv.recuperar(config.Keys.FINGER_KEY)
              .then((autorizacao) => {
                if (autorizacao) {
                  this.loginSrv.ConfirmaFinger().then((resultado) => {
                    if (resultado) {
                      this.utilSrv.desativarBloqueioMsg();
                      this.loginSrv.onConectedChange.next(this.loginSrv.fillUser());
                    } else {
                      this.utilSrv.desativarBloqueioMsg();
                      this.utilSrv.alerta('Biometria não foi reconhecida');
                    }
                  });
                } else {
                  this.utilSrv.desativarBloqueioMsg();
                  this.loginSrv.onConectedChange.next(this.loginSrv.fillUser());
                  // this.loginSrv.VoltandoSaida = false;
                  // this.utilSrv.alerta('Favor informar a sua senha. Logon automático não ativado pelo usuário');
                }
              });
          } else {
            this.tokenSrv.doRenew(this.loginSrv.getParameter().aplication_name)
              .subscribe((data1: any) => {
                if (data1.expires_in === -1) {
                  if (this.netSrv.isNetworkConnected().connected) {
                    this.loginSrv.VoltandoSaida = false;
                    this.storageSrv.Gravar(config.Keys.USUARIO_SAIU, true);
                    this.utilSrv.alerta(config.mensagemGenerica.TOKEN_RENEW_ERROR);
                    this.errorSrv.add(config.Error.TOKEN_EXPIRED, 'Renew Error');  // Tratar null
                    this.analyticsSrv.sendCustomEvent('Token Renew error', data1);
                  } else {
                    this.netSrv.NoNetworkMessage();
                  }
                } else {
                  this.tokenSrv.setToken(data1);
                  this.loginSrv.onConectedChange.next(this.loginSrv.fillUser());
                }
              });
            this.loginSrv.onConectedChange.next();
          }
        } else {
          this.loginSrv.onConectedChange.next(this.userSrv.empty());
          this.errorSrv.add(config.Error.TOKEN_NULL, 'Token nulo on inválido');  // Tratar null
        }
      }
    });
  }

  /**
   * Função responsavel pelo processo de logon do usuário.
   * Existe o tratamento de login automático que também é feito nesta função.
   */
  logar(ev) {
    console.log(ev);
    if (this.loginSrv.VoltandoSaida) {
      if (!this.userChanged) {
        this.storageSrv.recuperar(config.Keys.FINGER_KEY).then((resultado: boolean) => {
          if (resultado) {
            this.loginAutomatico();
          } else {
            this.utilSrv.alerta(config.mensagemGenerica.USUARIO_OU_SENHA_INVALIDOS)
          }
        });
      } else {
        this.loginSrv.login(this.usuario.UserName, this.usuario.UserPass)
          .subscribe(x => {
            console.log('1UBSRIBE DA FUNÇÃO LOGAR EM LOGINPAGE!', x);
          });
      }
    } else {
      this.storageSrv.recuperar(config.Keys.USUARIO_SAIU)
        .then((opcaoUsuarioSaiu: boolean) => {
          if (opcaoUsuarioSaiu) {
            this.loginSrv.login(this.usuario.UserName, this.usuario.UserPass)
              .subscribe(x => {
                this.storageSrv.Gravar(config.Keys.NOMEUSUARIO, this.usuario.UserName);
                console.log('3SUBSRIBE DA FUNÇÃO LOGAR EM LOGINPAGE!', x);
              });
          } else {
            this.storageSrv.recuperar(config.Keys.FINGER_KEY)
              .then((res: boolean) => {
                if (res) {
                  this.loginAutomatico();
                } else {
                  this.loginSrv.login(this.usuario.UserName, this.usuario.UserPass)
                    .subscribe(x => {
                      this.storageSrv.Gravar(config.Keys.NOMEUSUARIO, this.usuario.UserName);
                      console.log('3SUBSRIBE DA FUNÇÃO LOGAR EM LOGINPAGE!', x);
                    });
                }
              });
          }
        });
    }
  }

}
