

import { Injectable, ComponentFactoryResolver } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { Observer } from 'rxjs/Observer';
import { LoginService } from './login.service';
import { UtilService } from './util.service';


@Injectable()
export class BaseService {


    private baseUrl = "";

    constructor(private http: HttpClient,
        private loginService: LoginService,
        private uiServices: UtilService) {

    }

    setBaseUrl(url: string) {
        this.baseUrl = url;
    }
    getToken() {

        return this.loginService.tokenSrv.getToken().access_token;
    }
    getServiceUrl(service: string, action: string): string {
        return this.baseUrl + '/' + service + '/' + action;
    }

    executePost(service: string, action: string, parameter: any, options?: any): Observable<any> {

        return Observable.create(

            (observer: Observer<any>) => {

                const url = this.getServiceUrl(service, action)

                this.http.post(url, parameter, this.getHttpOptions(options)).subscribe(
                    (response: any) => {

                        const data = response;
                        observer.next(data);
                    },

                    (error) => {
                        if (error instanceof HttpErrorResponse) {
                            if (error.status === 401 || error.status === 403) {// houve um erro de acesso
                                this.loginService.tokenSrv.doRenew2().subscribe(// tenta fazer um renew do token
                                    resp => {
                                        if (resp["access_token"] == '')//o método não conseguiu renovar o token
                                        {
                                            this.uiServices.alerta('Não foi possivel atualizar o token. Será necessário fazer um novo login.', 5000);

                                            this.goToLogin();
                                            observer.error('Não foi possível renovar o token');
                                        }
                                        else {// teve sucesso no renew, vai chamar o metodo de negocio novamente

                                            console.log('seguiu chamando metodo');
                                            this.http.post(url, parameter, this.getHttpOptions(options)
                                            ).subscribe(
                                                resp =>
                                                    // teve sucesso no metodo de negocio, retorna resultado
                                                    observer.next(resp),
                                                error => {
                                                    //teve falha na execucao do metodo, retorna o erro

                                                    observer.error(error);
                                                }
                                            );
                                        }
                                    },
                                    error => {
                                        observer.error('Não foi possível renovar o token');
                                    });
                            }
                            else
                                observer.error(error);
                        }
                        else
                            observer.error(error);
                    }
                );

            }
        );
    }
    executePut(service: string, action: string, parameter: any, options?: any): Observable<any> {

        return Observable.create(

            (observer: Observer<any>) => {

                const url = this.getServiceUrl(service, action)

                this.http.put(url, parameter, this.getHttpOptions(options)).subscribe(
                    (response: any) => {

                        const data = response;
                        observer.next(data);

                    },

                    (error) => {
                        if (error instanceof HttpErrorResponse) {
                            if (error.status === 401 || error.status === 403) {// houve um erro de acesso
                                this.loginService.tokenSrv.doRenew2().subscribe(// tenta fazer um renew do token
                                    resp => {
                                        if (resp["access_token"] == '')//o método não conseguiu renovar o token
                                        {
                                            this.uiServices.alerta('Não foi possivel atualizar o token. Será necessário fazer um novo login.', 5000);

                                            this.goToLogin();
                                            observer.error('Não foi possível renovar o token');
                                        }
                                        else {// teve sucesso no renew, vai chamar o metodo de negocio novamente

                                            console.log('seguiu chamando metodo');
                                            this.http.put(url, parameter, this.getHttpOptions(options)).subscribe(
                                                resp =>
                                                    // teve sucesso no metodo de negocio, retorna resultado
                                                    observer.next(resp),
                                                error => {
                                                    //teve falha na execucao do metodo, retorna o erro

                                                    observer.error(error);
                                                }
                                            );
                                        }
                                    },
                                    error => {
                                        observer.error('Não foi possível renovar o token');
                                    });
                            }
                            else
                                observer.error(error);
                        }
                        else
                            observer.error(error);
                    }
                );

            }
        );
    }
    executePatch(service: string, action: string, parameter: any, options?: any): Observable<any> {

        return Observable.create(

            (observer: Observer<any>) => {

                const url = this.getServiceUrl(service, action)

                this.http.patch(url, parameter, this.getHttpOptions(options)).subscribe(
                    (response: any) => {

                        const data = response;
                        observer.next(data);

                    },

                    (error) => {
                        if (error instanceof HttpErrorResponse) {
                            if (error.status === 401 || error.status === 403) {// houve um erro de acesso
                                this.loginService.tokenSrv.doRenew2().subscribe(// tenta fazer um renew do token
                                    resp => {
                                        if (resp["access_token"] == '')//o método não conseguiu renovar o token
                                        {
                                            this.uiServices.alerta('Não foi possivel atualizar o token. Será necessário fazer um novo login.', 5000);

                                            this.goToLogin();
                                            observer.error('Não foi possível renovar o token');
                                        }
                                        else {// teve sucesso no renew, vai chamar o metodo de negocio novamente

                                            console.log('seguiu chamando metodo');
                                            this.http.patch(url, parameter, this.getHttpOptions(options)).subscribe(
                                                resp =>
                                                    // teve sucesso no metodo de negocio, retorna resultado
                                                    observer.next(resp),
                                                error => {
                                                    //teve falha na execucao do metodo, retorna o erro

                                                    observer.error(error);
                                                }
                                            );
                                        }
                                    },
                                    error => {
                                        observer.error('Não foi possível renovar o token');
                                    });
                            }
                            else
                                observer.error(error);
                        }
                        else
                            observer.error(error);
                    }
                );
            }
        );
    }
    executeGet(service: string, action: string, options?: any, disableAutoLogout?: boolean, disableAutoRenew?: boolean): Observable<any> {

        return Observable.create(

            (observer: Observer<any>) => {
                console.log('Iniciou executeGet');

                const url = this.getServiceUrl(service, action)

                this.http.get(url, this.getHttpOptions(options)).subscribe(
                    (response: any) => {
                      //  console.log('Teve resposta no  executeGet');
                        const data = response;
                        observer.next(data);

                    },

                    (error) => {
                        //console.log('Erro no  executeGet');
                        if (error instanceof HttpErrorResponse && !disableAutoRenew)  {
                            if (error.status === 401 || error.status === 403) {// houve um erro de acesso
                              //  console.log('Erro de autorizacao, vai fazer renew');
                                this.loginService.tokenSrv.doRenew2().subscribe(// tenta fazer um renew do token
                                    resp => {
                                        if (resp["access_token"] == '')//o método não conseguiu renovar o token
                                        {
                                            console.log('Não foi possivel atualizar o token. Será necessário fazer um novo login.');
                                            this.uiServices.alerta('Não foi possivel atualizar o token. Será necessário fazer um novo login.', 5000);
                                            if (!disableAutoLogout)
                                                this.goToLogin();
                                            observer.error('Não foi possível renovar o token');
                                        }
                                        else {// teve sucesso no renew, vai chamar o metodo de negocio novamente

                                            console.log('seguiu chamando metodo. renew com sucesso');
                                            this.http.get(url, this.getHttpOptions(options)).subscribe(
                                                resp =>
                                                    // teve sucesso no metodo de negocio, retorna resultado
                                                    observer.next(resp),
                                                error => {
                                                    //teve falha na execucao do metodo, retorna o erro

                                                    observer.error(error);
                                                }
                                            );
                                        }
                                    },
                                    error => {
                                        observer.error('Não foi possível renovar o token');
                                    });
                            }
                            else
                                observer.error(error);
                        }
                        else
                            observer.error(error);
                    }
                );

            }
        );
    }
    executeDelete(service: string, action: string, options?: any): Observable<any> {

        return Observable.create(

            (observer: Observer<any>) => {

                const url = this.getServiceUrl(service, action)

                this.http.delete(url, this.getHttpOptions(options)).subscribe(
                    (response: any) => {

                        const data = response;
                        observer.next(data);

                    },

                    (error) => {
                        if (error instanceof HttpErrorResponse) {
                            if (error.status === 401 || error.status === 403) {// houve um erro de acesso
                                this.loginService.tokenSrv.doRenew2().subscribe(// tenta fazer um renew do token
                                    resp => {
                                        if (resp["access_token"] == '')//o método não conseguiu renovar o token
                                        {
                                            this.uiServices.alerta('Não foi possivel atualizar o token. Será necessário fazer um novo login.', 5000);

                                            this.goToLogin();
                                            observer.error('Não foi possível renovar o token');
                                        }
                                        else {// teve sucesso no renew, vai chamar o metodo de negocio novamente

                                            console.log('seguiu chamando metodo');
                                            this.http.delete(url, this.getHttpOptions(options)).subscribe(
                                                resp =>
                                                    // teve sucesso no metodo de negocio, retorna resultado
                                                    observer.next(resp),
                                                error => {
                                                    //teve falha na execucao do metodo, retorna o erro

                                                    observer.error(error);
                                                }
                                            );
                                        }
                                    },
                                    error => {
                                        observer.error('Não foi possível renovar o token');
                                    });
                            }
                            else
                                observer.error(error);
                        }
                        else
                            observer.error(error);
                    }
                );

            }
        );
    }
    goToLogin() {
        this.loginService.logout();
    }

    getHttpOptions(options: any): any {
       // console.log('Entrou no getHttpOptions');
        if (!this.getToken()) {
            console.log('Não havia token disponível');
        };
        let newOptions: any;

        if (!options) {
            newOptions = {
                headers: new HttpHeaders({ "Authorization": "Bearer " + this.getToken() })
            };
        } else {
            if (options.headers) {
                newOptions = options;
                let newHeaders = new HttpHeaders();

                let actualHeaders = options.headers as HttpHeaders;
                let authorizationFound = false;
                actualHeaders.keys().forEach(element => {
                    if (element == 'Authorization') {

                        newHeaders = newHeaders.append('Authorization', "Bearer " + this.getToken());
                        console.log(newHeaders.get('Authorization'));
                        authorizationFound = true;
                    }
                    else {
                        console.log('chave: ' + element + ' Valor: ' + actualHeaders.get(element));
                        newHeaders = newHeaders.append(element, actualHeaders.get(element));
                    }
                });
                if (!authorizationFound)
                    newHeaders = newHeaders.append('Authorization', "Bearer " + this.getToken());

                newOptions.headers = newHeaders;
            }
            else {
                newOptions = {
                    headers: new HttpHeaders({ "Authorization": "Bearer " + this.getToken() })
                };
            }
        }
        //console.log('Options: ' + JSON.stringify(newOptions));
        return newOptions;
    }


}

