import { Injectable } from '@angular/core';

/** INTERFACES */
import * as Status from './../interfaces/status.model';

/** CONFIG */
import { Config as config } from './../environment/config';


import { Subject } from 'rxjs/Subject';

/**
 * Classe responsavel por todo o tratamento de erro do app.
 * A identificação dos erros e warnings é feita com base no numero do mesmo.
 * Esta numeração esta contida no arquivo de configuração e inicialmente esta configiurada para:
 *
 *      LIMITEWARNINGMENOR : 6000,
 *      LIMITEERRORMENOR : 4000,
 *      LIMITEERROMAIOR : 6000
 * 
 */
@Injectable()
export class ErrorService {

  private _table: Status.Status[] = [];
  private _limiteWarningMenor = config.Error.LIMITEWARNINGMENOR;
  private _limiteErrorMenor = config.Error.LIMITEERRORMENOR;
  private _limiteErrorMaior = config.Error.LIMITEERROMAIOR;

  public onError = new Subject<Status.Status>();

  constructor() {
    this._table = [];
  }

  /**
   * Retorna a lista de erros ocorridos na sessão em curso
   */
  public errorList() {
    return this._table.sort(this.SortErrorListByData);
  }

  /**
   * Retorna o último erro ocorrido
   */
  public lastError(): Status.Status {
    if (this._table.length > 0) {
      return this._table[this._table.length - 1];
    } else {
      return <Status.Status>{ Code: -1, Description: '', data: null, isError: false, isWarning: false };
    }
  }

  /**
   * Limpa a lista de erros ocorridos
   */
  private clearErrorList() {
    this._table = [];
  }

  /**
   * Metodo publico de limpeza da lista de erros
   */
  public clear() {
    this.clearErrorList();
  }

  /**
   * Adiciona um evento de erro a lista de erros da sessão
   * 
   * @param code Codigo do erro
   * @param description Descrição do erro
   */
  public add(code: number, description: string): boolean {
    let retorno: boolean;
    if ((code >= 0) && (code <= 1000)) {
      this._table.push(<Status.Status>{
        Code: code, Description: description, data: new Date(),
        isError: this.isError(code), isWarning: this.isWarning(code)
      });
      this.onError.next(this.lastError());
      retorno = true;
    } else {
      this._table.push(<Status.Status>{
        Code: 10, Description: '', data: new Date(),
        isError: this.isError(code), isWarning: this.isWarning(code)
      });
      retorno = false;
    }
    return retorno;
  }

  /**
   * Exibe um erro na tela via toast
   */
  public showError() {
    // problemas no toast de dentro de um package
  }

  /**
   * Com base na classificação definida na documentação do sistema, informa se o erro é um warning
   * @param code Codigo a ser validado
   */
  private isWarning(code) {
    return (code > this._limiteWarningMenor);
  }

  /**
   * * Com base na classificação definida na documentação do sistema, informa se o erro é um ERRO
   * @param code Codigo a ser validado
   */
  private isError(code) {
    return ((code >= this._limiteErrorMenor) && (code >= this._limiteErrorMaior));
  }

  /**
   * Ordena os erros por data do mais recente para o mais antigo
   * 
   * @param x Interno
   * @param y Interno
   */
  private SortErrorListByData(x, y) {
    const sortColumnName = 'data';
    return ((x[sortColumnName] === y[sortColumnName]) ? 0 : ((x[sortColumnName] < y[sortColumnName]) ? 1 : -1));
  }

}
