
import { Injectable } from '@angular/core';
import { Platform, Config, NavController, PopoverController } from 'ionic-angular';
// import { Storage } from '@ionic/storage';
import { AlertController } from 'ionic-angular';
import { FingerprintAIO } from '@ionic-native/fingerprint-aio';

import { App } from 'ionic-angular';


import { HttpClient } from '@angular/common/http';
import { UtilService } from './util.service';

import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/observable/of';

// import * as jwt from 'jwt-decode';
// import * as jwtClaims from 'jwt-claims';

/** NOSSOS PACOTES */
import { AnalyticsService } from '@ons/ons-mobile-analytics';

/** PAGES */
import { LoginPage } from '../Pages/login/login';


/** SERVICES */
import { TokenService } from './token.service';
import { ErrorService } from './error.service';
import { UserService } from './user.service';
import { StorageService } from './storage.service';
import { EnvironmentService } from './environment.service';
import { NetWorkService } from './network.service';

/** INTERFACES */
import * as User from './../interfaces/user.model';
import * as NetState from './../interfaces/network.model';
import * as Parameter from './../interfaces/parametros.model';

/** CONFIG */
import { Config as config } from './../environment/config';
import { Observer } from 'rxjs';



/**
 * Esta classe é o coração do componente.
 * Nela são processadas as autenticações e validações de usuário. Todos os serviços são consumidos a partir
 * deste ponto.
 * 
 * O evento onConectedChange é setado aqui sempre que o usuário é autenticado, passando o objeto usuário ativo.
 * 
 * 
 * --- 26-09-2018
 * 1- Mensagem   USUARIO_VERIFICAR_SENHA: 'Por favor, verifique usuário e senha.' deixa de ser usada.
 *
 * 
 */
@Injectable()
export class LoginService {

  public onConectedChange = new Subject<User.User>();

  private _parameters: Parameter.Parameters = <Parameter.Parameters>{ aplication_name: '' };
  private _currentUser: User.User;
  public VoltandoSaida = false;
  public Usuario_alterado = false;
  public press_exit = false;


  /**
   * Retorna o objeto do usuário em curso
   */
  get CurrentUser(): User.User {
    return this.userSrv.User;
  }

  // get VoltandoSaida() {
  //   this.storageSrv.recuperar(config.Keys.autoLogin)
  //     .then((resultado) => {
  //       return resultado;
  //     });
  // }

  constructor(public utilSrv: UtilService,
    public tokenSrv: TokenService,
    public http: HttpClient,
    public userSrv: UserService,
    public analyticsSrv: AnalyticsService,
    public netSrv: NetWorkService,
    public errorSrv: ErrorService,
    public storageSrv: StorageService,
    public app: App,
    public fingerAuth: FingerprintAIO,
    public configSrv: Config,
    public platform: Platform,
    public envSrv: EnvironmentService,
    public alertCtrl: AlertController,
    public popoverCtrl: PopoverController
  ) {
    this._currentUser = userSrv.empty();
  }
  /**
 * Mostra opções de sair
 * @param pageName Nome página para registrar no log
 */
  public showLogoutOptions(pageName: string, showBack: boolean, callbackVoltar) {
    let buttons = [];
    if (showBack) {
      buttons.push({
        text: 'Voltar',
        handler: () => {
          this.analyticsSrv.sendCustomEvent(pageName + ' - logoutOptions - Voltar');
          callbackVoltar.emit("voltar");
        }
      })
    }
    buttons.push({
      text: 'Sair',
      handler: () => {
        this.analyticsSrv.sendCustomEvent(pageName + ' - logoutOptions - Sair');
        this.logout();
      }
    });
    buttons.push({
      text: 'Encerrar',
      handler: () => {
        this.analyticsSrv.sendCustomEvent(pageName + ' - logoutOptions - Encerrar');
        this.exitApp();
      }
    })
    buttons.push({
      text: 'Cancelar',
      handler: () => {
        this.analyticsSrv.sendCustomEvent(pageName + '  - logoutOptions - Cancelar');

      }
    });
    const confirmation = this.alertCtrl.create({
      title: 'Opções de saída',
      message: 'Você pode Voltar, Sair e entrar facilmente na próxima vez ou Encerrar, removendo seus dados de login.',
      buttons: buttons
    });

    confirmation.present();
  }

  /**
   * Seta o nome da aplicação a ser utilizado na autenticação com o POP
   * @param app Nome da aplicação
   */
  public setAplicationName(app: string) {
    this._parameters.aplication_name = app;
    this.tokenSrv.setClientId(app);
  }
  /**
   * Obtem o nome da aplicação a ser utilizado na autenticação com o POP
   */
  public getApplicationName() {
    return this._parameters.aplication_name
  }
  /**
   * Executa a autenticação do usuário
   * @param user Usuário
   * @param pass Senha
   */
  public login(user: string, pass: string): Observable<any> {
    if (this._parameters.aplication_name === '') {
      this.analyticsSrv.sendNonFatalCrash(config.mensagemGenerica.ERRO_FALTA_NOME_APLICACAO, '');
      this.errorSrv.add(config.Error.PARAMETER_ERROR, config.mensagemGenerica.ERRO_FALTA_NOME_APLICACAO);
      throw new Error(config.mensagemGenerica.ERRO_FALTA_NOME_APLICACAO);
    } else {
      this.validarUsuario(user, pass)
        .subscribe(result => {
          switch (result.status) {
            case config.userValidate.USUARIO_OU_SENHA_INVALIDOS:
              this.analyticsSrv.sendCustomEvent(config.mensagemGenerica.USUARIO_OU_SENHA_INVALIDOS, result);
              this.utilSrv.alerta(result.mensagem);
              break;
            case config.userValidate.LOGIN_DIGITAL_PADRAO:
              break;
            case config.userValidate.LOGIN_DIGITAL_SENHA_EM_BRANCO:
              this.utilSrv.alerta(result.mensagem);
              break;
            case config.userValidate.LOGIN_DIGITAL_SENHA_EM_BRANCO:
              this.utilSrv.alerta(result.mensagem);
              break;
            case config.userValidate.DADOS_VALIDOS:
              this.utilSrv.ativarBloqueioMsg(config.mensagemGenerica.ENTRANDO);
              this.autenticarPOP(user, pass)
                .subscribe(res => {
                  if (res) {
                    this.storageSrv.deletar(config.Keys.USUARIO_SAIU);
                    this.storageSrv.recuperar(config.Keys.FINGER_KEY)
                      .then((autorizacao) => {
                        if (!autorizacao || autorizacao === undefined || autorizacao === null) {
                          if (this.platform.is('cordova')) {
                            this.utilSrv.desativarBloqueioMsg();
                            this.fingerAuth.isAvailable().then((valor) => {
                              let alert = this.alertCtrl.create({
                                title: config.mensagemGenerica.AUTENTICAR_BIOMETRIA,
                                message: config.mensagemGenerica.AUTENTICAR_BIOMETRIA_CONFIRMAR,
                                buttons: [
                                  {
                                    text: 'Não',
                                    role: 'cancel',
                                    handler: () => {
                                      this.storageSrv.Gravar(config.Keys.FINGER_KEY, false);
                                    }
                                  },
                                  {
                                    text: 'Sim',
                                    handler: () => {
                                      this.ConfirmaFinger().then((confirmado) => {

                                        if (confirmado) {
                                          this.storageSrv.Gravar(config.Keys.FINGER_KEY, true);
                                          this.utilSrv.
                                            alerta(config.mensagemGenerica.AUTENTICAR_BIOMETRIA_OK, 4000,
                                              'middle', 'Autenticação digital!');
                                        } else {
                                          this.storageSrv.Gravar(config.Keys.FINGER_KEY, false);
                                          this.utilSrv.
                                            alerta(
                                              config.mensagemGenerica.AUTENTICAR_BIOMETRIA_ERRO, 4000, 'middle', 'Autenticação digital!');
                                        }
                                      });
                                    }
                                  }
                                ]
                              });
                              alert.present();
                            }).catch((err) => {
                              this.storageSrv.Gravar(config.Keys.FINGER_KEY, false);
                              this.analyticsSrv.sendNonFatalCrash(config.mensagemGenerica.FINGER_ID_UNSUPORTED, err);
                              this.errorSrv.add(config.Error.FINGER_ID_UNSUPORTED, err);
                              return false;
                            });
                          } else {
                            this.storageSrv.Gravar(config.Keys.FINGER_KEY, false);
                            this.utilSrv.desativarBloqueioMsg();
                          }
                        } else {
                          this.utilSrv.desativarBloqueioMsg();
                        }
                      });
                  } else {
                    if (!this.netSrv.isNetworkConnected().connected) {
                      this.analyticsSrv.sendNonFatalCrash(config.mensagemGenerica.USUARIO_OU_SENHA_INVALIDOS, '');
                    } else {
                      this.utilSrv.alerta(config.mensagemGenerica.USUARIO_OU_SENHA_INVALIDOS);
                    }
                    this.utilSrv.desativarBloqueioMsg();
                  }
                  this.VoltandoSaida = false;
                  // this.utilSrv.desativarBloqueioMsg();
                }, (err) => {
                  let message: string;
                  if (err.status && (err.status === 401 || err.status === 400)) {
                    message = config.mensagemGenerica.USUARIO_NAO_AUTORIZADO + ' erro: ' + err;
                  } else {
                    this.analyticsSrv.sendNonFatalCrash(config.mensagemGenerica.AUTHENTICATION_ERROR_POP, err);
                    this.errorSrv.add(config.Error.AUTHENTICATION_ERROR_POP, message);
                  }
                  this.utilSrv.desativarBloqueioMsg();
                  this.analyticsSrv.sendNonFatalCrash(config.mensagemGenerica.USUARIO_OU_SENHA_INVALIDOS, '');
                  this.utilSrv.alerta(config.mensagemGenerica.USUARIO_OU_SENHA_INVALIDOS, 4000, 'top', 'Não foi possível autenticar!');
                }); break;
          }
        });
      /**
       * Este retorno não é relevante nesta versão pois o tratamento é feito com o Subject 
       */
      return Observable.of(true);
    }
  }


  /**
   * Confirma se o usuário deseja utilizar ou não o finger autentication
   * Somente será exibido se o device possuir este recurso.
   */
  public ConfirmaFinger(): Promise<boolean> {
    return this.fingerAuth.isAvailable().then((valor) => {
      return this.fingerAuth.show({
        clientId: 'keychain.br.org.ons',
        clientSecret: 'password', // Only necessary for Android
        disableBackup: true,  // Only for Android(optional)
        localizedReason: 'Toque no sensor para entrar' // Only for iOS
      }).then((result: any) => {
        return true;
      })
        .catch((err: any) => {
          this.analyticsSrv.sendNonFatalCrash(config.mensagemGenerica.FINGER_ID_ERROR, err);
          this.errorSrv.add(config.Error.FINGER_ID_ERROR, err);
          return false;
        });

    }).catch((err) => {
      this.analyticsSrv.sendNonFatalCrash(config.mensagemGenerica.FINGER_ID_UNSUPORTED, err);
      this.errorSrv.add(config.Error.FINGER_ID_UNSUPORTED, err);
      return false;
    });
  }

  /**
   * Faz a autenticação do usuário no POP
   * Os dados de usuário e senha NÃO SÃO ARMAZENADOS.
   * 
   * @param user Nome do usuário a ser autenticado
   * @param pass Senha do usuário a ser autenticado
   */
  public autenticarPOP(user: string, pass: string): Observable<boolean> {
    let connected: boolean;
    let values: any = {
      username: user
      , password: pass
      , client_id: this._parameters.aplication_name
      , grant_type: config.GRANT_TYPE
    };
    let result: boolean;
    if (this.netSrv.isNetworkConnected().connected) {
      const body = Object.keys(values)
        .map((key) => { return encodeURIComponent(key) + '=' + encodeURIComponent(values[key]); }).join('&');
      return this.http.post(this.envSrv.current.value + '/token', body, this.utilSrv.httpOptions)
        .map(
          data => {
            this.tokenSrv.setToken(data);
            this.storageSrv.Gravar(config.Keys.tokenUser, data);
            this.storageSrv.Gravar(config.Keys.autoLogin, config.VoltandoSaida.INICIALIZADO);
            this.onConectedChange.next(this.fillUser());
            result = true;
            return true;
          },
          (err) => {
            this.analyticsSrv.sendNonFatalCrash(config.mensagemGenerica.AUTHENTICATION_ERROR_POP, err);
            this.utilSrv.desativarBloqueioMsg();
            this.utilSrv.ErrorAlerta('err', 2000, 'middle', 'Erro no Pop');
            this.errorSrv.add(config.Error.AUTHENTICATION_ERROR_POP, 'Erro de autenticação');
            result = false;
            return false;

          });
    } else {
      this.utilSrv.alerta(config.NetWorkMessages.SEM_CONEXAO);
      result = false;
      return Observable.of(false);
    }
  }

  /**
   * Faz o processo de validação do token com o qual o sistema executa o direcionamento automático para o
   * page destino.
   */
  public validateTokenFlow(): Promise<any> {
    return this.tokenSrv.getTokenFromStorage()
      .then(data => {
        return data;
      }).catch((err) => {
        this.analyticsSrv.sendNonFatalCrash(config.mensagemGenerica.TOKEN_ERROR, err);
        //  this.onStorageError.next(err);
        return null;
      });
  }


  /**
   * Preenche o objeto usuário com os dados do token
   *
   */
  public fillUser(): User.User {
    let u: User.User = this.userSrv.empty();
    u.email = this.tokenSrv.getDecodedByName(config.Claim_Email);
    u.User_Full_Name = this.tokenSrv.getDecodedByName(config.Claim_UserFullName);
    u.User_Name = this.tokenSrv.getDecodedByName(config.Claim_Account);
    u.Connected_At = new Date();
    u.Connected = true;
    this.userSrv.setUser(u);
    this._currentUser = u;
    return u;
  }

  /**
   * Desconecta o usuário da sessão atual
   */
  public logout() {
    this.press_exit = true;
    this.VoltandoSaida = true;
    this.storageSrv.Gravar(config.Keys.USUARIO_SAIU, true);

    this.userSrv.setUser(this.userSrv.empty());
    this.app.getActiveNav().setRoot(LoginPage);
  }

  /**
   * Retira o usuário do app.
   * Todos os dados da execução são cancelados 
   * Os dados armazenados não são alterados.
   */
  public exitApp() {
    this.VoltandoSaida = false;
    this.storageSrv.limparBase();
    this.storageSrv.deletar(config.Keys.USUARIO_SAIU);
    this.userSrv.setUser(this.userSrv.empty());
    this.storageSrv.deletar(config.Keys.autoLogin); //, config.VoltandoSaida.VOLTANDO);
    this.app.getActiveNav().setRoot(LoginPage);
  }

  /**
   * Retorna o objeto com os parâmetros do componente
   */
  public getParameter(): Parameter.Parameters {
    return this._parameters;
  }

  /**
   * Faz a validação do usuário para autenticação
   * Este metodo não autentica o usuário, apenas faz a verificação dos dados básicos
   *
   * @param usuario Usuário a ser validado
   * @param senha Senha do usuário a ser validada
   * 
   */
  validarUsuario(usuario: string, senha: string): Observable<any> {
    let result: any = {};
    // result.mensagemArray = []
    result.status = 'VALIDO';
    result.mensagem = '';
    try {
      if (this.VoltandoSaida) {
        if (usuario && !senha) {
          if (this.Usuario_alterado) {
            result = {
              'mensagem': config.mensagemGenerica.USUARIO_E_SENHA_ALTERADOS, 'status': config.userValidate.USUARIO_OU_SENHA_INVALIDOS
            };
          } else {
            result = {
              'mensagem': config.mensagemGenerica.USUARIO_SENHA_OK, 'status': config.userValidate.DADOS_VALIDOS
            }
          }
        } else {
          if (usuario && senha) {
            result = {
              'mensagem': config.mensagemGenerica.USUARIO_SENHA_OK, 'status': config.userValidate.DADOS_VALIDOS
            };
          } else if (!usuario && senha) {
            result = {
              'mensagem': config.mensagemGenerica.USUARIO_NAOINFORMADO, 'status': config.userValidate.USUARIO_OU_SENHA_INVALIDOS
            };
          } else {
            if (!usuario && !senha) {
              result = {
                'mensagem': config.mensagemGenerica.USUARIO_SENHA_NAOINFORMADOS, 'status': config.userValidate.USUARIO_OU_SENHA_INVALIDOS
              };
            }
          }
        }
      } else {
        if (!usuario && !senha) {
          result = {
            'mensagem': config.mensagemGenerica.USUARIO_SENHA_NAOINFORMADOS,
            'status': config.userValidate.USUARIO_OU_SENHA_INVALIDOS
          };
        } else {
          if (!usuario && senha) {
            result = {
              'mensagem': config.mensagemGenerica.USUARIO_NAOINFORMADO, 'status': config.userValidate.USUARIO_OU_SENHA_INVALIDOS
            };
          } else {
            if (usuario && !senha) {
              result = {
                'mensagem': config.mensagemGenerica.USUARIO_SENHA_BRANCO, 'status': config.userValidate.USUARIO_OU_SENHA_INVALIDOS
              };
            } else {
              if (usuario && senha) {
                result = {
                  'mensagem': config.mensagemGenerica.USUARIO_SENHA_OK, 'status': config.userValidate.DADOS_VALIDOS
                };
              }
            }
          }
        }
      }
      return Observable.of(result);
    } catch (error) {
      this.analyticsSrv.sendNonFatalCrash(config.mensagemGenerica.USUARIO_ERRO_VALIDACAO, error);
    }
  }


  /**
  * Executa o logon automático quando o usuário retorna à aplicação.
  * O token arazenado é utilizado neste processo
  */
  public loginAutomatico() {
    this.validateTokenFlow().then(data => {
      console.log('loginautomatico 1');
      if (data === null) {
        console.log('loginautomatico 2');
        this.onConectedChange.next(this.userSrv.empty());
        this.errorSrv.add(config.Error.TOKEN_NULL, 'Token nulo on inválido');  // Tratar null
      } else {
        console.log('loginautomatico 3');
        if (this.tokenSrv.isValid(data)) {
          console.log('loginautomatico 4');
          if (!this.tokenSrv.isExpired(data)) {
            console.log('loginautomatico 5');
            this.tokenSrv.setToken(data);
            this.storageSrv.recuperar(config.Keys.FINGER_KEY)
              .then((autorizacao) => {
                console.log('loginautomatico 6');
                if (autorizacao) {
                  console.log('loginautomatico 7');
                  this.ConfirmaFinger().then((resultado) => {
                    console.log('loginautomatico 8');
                    if (resultado) {
                      console.log('loginautomatico 9');
                      this.utilSrv.desativarBloqueioMsg();
                      this.onConectedChange.next(this.fillUser());
                    } else {
                      console.log('loginautomatico 10');
                      this.utilSrv.desativarBloqueioMsg();
                      this.utilSrv.alerta('Biometria não foi reconhecida');
                    }
                  });
                  console.log('loginautomatico 11');
                } else {
                  console.log('loginautomatico 12');
                  this.utilSrv.desativarBloqueioMsg();
                  this.onConectedChange.next(this.fillUser());
                }
              });
            console.log('loginautomatico 13');
          } else {
            console.log('loginautomatico 14');
            this.tokenSrv.doRenew(this.getParameter().aplication_name)
              .subscribe((data1: any) => {
                console.log('loginautomatico 15');
                if (data1.expires_in === -1) {
                  console.log('loginautomatico 16');
                  if (this.netSrv.isNetworkConnected().connected) {
                    console.log('loginautomatico 17');
                    this.VoltandoSaida = false;
                    this.storageSrv.Gravar(config.Keys.USUARIO_SAIU, true);

                    this.app.getActiveNav().setRoot(LoginPage);
                    console.log('loginautomatico 18');

                  } else {
                    console.log('loginautomatico 19');
                    this.netSrv.NoNetworkMessage();
                  }
                } else {
                  console.log('loginautomatico 20');
                  this.tokenSrv.setToken(data1);
                  this.onConectedChange.next(this.fillUser());
                }
              });
            console.log('loginautomatico 21');
            this.onConectedChange.next();
          }
        } else {
          console.log('loginautomatico 22');
          this.onConectedChange.next(this.userSrv.empty());
          this.app.getActiveNav().setRoot(LoginPage);
        }
      }
    });
  }
  public loginAutomatico2(): Observable<any> {

    return Observable.create(

      (observer: Observer<any>) => {

        this.validateTokenFlow().then(data => {
          console.log('loginautomatico 1');
          if (data === null) {
            console.log('loginautomatico 2');
            observer.error('Token nulo on inválido');
            this.errorSrv.add(config.Error.TOKEN_NULL, 'Token nulo on inválido');  // Tratar null
          } else {
            console.log('loginautomatico 3');
            if (this.tokenSrv.isValid(data)) {
              console.log('loginautomatico 4');
              if (!this.tokenSrv.isExpired(data)) {
                console.log('loginautomatico 5');
                this.tokenSrv.setToken(data);
                this.storageSrv.recuperar(config.Keys.FINGER_KEY)
                  .then((autorizacao) => {
                    console.log('loginautomatico 6');
                    if (autorizacao) {
                      console.log('loginautomatico 7');
                      this.ConfirmaFinger().then((resultado) => {
                        console.log('loginautomatico 8');
                        if (resultado) {
                          console.log('loginautomatico 9');
                          this.utilSrv.desativarBloqueioMsg();
                          observer.next(this.fillUser());
                        } else {
                          console.log('loginautomatico 10');
                          this.utilSrv.desativarBloqueioMsg();
                          this.utilSrv.alerta('Biometria não foi reconhecida');
                          //this.app.getActiveNav().setRoot(LoginPage);
                          observer.error('Biometria não foi reconhecida');
                        }
                      });
                      console.log('loginautomatico 11');
                    } else {
                      console.log('loginautomatico 12');
                      this.utilSrv.desativarBloqueioMsg();
                      observer.next(this.fillUser());
                    }
                  }).catch(err => observer.error(err));
                console.log('loginautomatico 13');
              } else {
                console.log('loginautomatico 14');
                this.tokenSrv.doRenew(this.getParameter().aplication_name)
                  .subscribe((data1: any) => {
                    console.log('loginautomatico 15');
                    if (data1.expires_in === -1) {
                      console.log('loginautomatico 16');
                      if (this.netSrv.isNetworkConnected().connected) {
                        console.log('loginautomatico 17');
                        this.VoltandoSaida = false;
                        this.storageSrv.Gravar(config.Keys.USUARIO_SAIU, true);

                        //this.app.getActiveNav().setRoot(LoginPage);
                        console.log('loginautomatico 18');
                        observer.error('loginautomatico 18');

                      } else {
                        console.log('loginautomatico 19');
                        this.netSrv.NoNetworkMessage();
                        observer.error('loginautomatico 19');
                      }
                    } else {
                      console.log('loginautomatico 20');
                      this.tokenSrv.setToken(data1);
                      observer.next(this.fillUser());
                    }
                  },
                    err => observer.error(err));
                console.log('loginautomatico 21');

              }
            } else {
              console.log('loginautomatico 22');
              observer.error('loginautomatico 22');
              //this.app.getActiveNav().setRoot(LoginPage);
            }
          }
        }
        ).catch(err => observer.error(err));
      })
  }
  /**
* Retorna se pode ser realizado o login automático (bool).
*/
  checkLoginStatus(): Observable<any> {

    return Observable.create(

      (observer: Observer<any>) => {


        this.storageSrv.recuperar(config.Keys.USUARIO_SAIU)
          .then((opcaoAuto: boolean) => {
            let autoLogin = false;
            this.storageSrv.recuperar(config.Keys.FINGER_KEY)
              .then((opcaoFinger: boolean) => {
                if ((!opcaoFinger) && (!opcaoAuto)) {
                  autoLogin = true;
                } else {
                  if ((!opcaoFinger) && (opcaoAuto)) {
                    autoLogin = false;
                  } else {
                    if ((opcaoFinger) && (opcaoAuto)) {
                      autoLogin = true;
                    } else {
                      if ((opcaoFinger) && (!opcaoAuto)) {
                        autoLogin = true;
                      }
                    }
                  }
                }
                if ((autoLogin) && (!this.VoltandoSaida)) {
                  // this.loginAutomatico();
                  observer.next(true);
                }
                else
                  observer.next(false);
              });
          }).catch(err => observer.error(err));

      }
    );
  }

}
