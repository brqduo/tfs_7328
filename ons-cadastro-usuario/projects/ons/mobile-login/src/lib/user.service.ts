import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/observable/of';

/** SERVICES */
import { ErrorService } from './error.service';
import { AnalyticsService } from '@ons/ons-mobile-analytics';

/** INTERFACES */
import * as User from './../interfaces/user.model';

/** CONFIG */
import { Config as config } from './../environment/config';


@Injectable()
export class UserService {

  private _user: User.User;

  public onUserChange = new Subject<User.User>();

  constructor(public errorSrv: ErrorService
    , public storage: Storage
    , public analyticsSrv: AnalyticsService) {
    this._user = this.empty();

  }

  get User(): User.User {
    if ((this._user === null) || (this._user == undefined)) {
      return this.empty();
    } else {
      this._user.Connected_Time = new Date().getTime() - this._user.Connected_At.getTime();
      return this._user;
    }
  }

  public setUser(u: User.User) {
    if (!this.isNull(u)) {
      this._user = u;
      this.onUserChange.next(u);
    } else {
      this.analyticsSrv.sendNonFatalCrash(config.mensagemGenerica.USUARIO_NULO_OU_INVALIDO, u);
      this.errorSrv.add(config.Error.USER_NULL, 'Set user null');
    }
  }

  public hasPermission(permission: string): boolean {
    return false;
    // Esta rotina sera incluida no componente de segurança
  }

  public listPermissions(): string[] {
    return null;
    // Esta rotina sera incluida no componente de segurança
  }

  public isNull(u: User.User) {
    if ((u === undefined) || (u === null)) {
      return true;
    } else {
      return false;
    }
  }

  public empty(): User.User {
    return <User.User>{
      User_Name: '', User_Full_Name: '', Connected_At: new Date(),
      Connected_Time: null, Connected: false, DeviceProperties: [], email: '', Claims: []
    };
  }

}
