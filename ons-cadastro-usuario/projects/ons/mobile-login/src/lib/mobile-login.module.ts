import { NgModule, ModuleWithProviders, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { MobileLoginComponent } from './mobile-login.component';
import { ErrorService } from './error.service';
import { LoginPage } from '../Pages/login/login';
import { IonicModule, Button } from 'ionic-angular';
import { LoginService } from './login.service';
import { TokenService } from './token.service';
import { UtilService } from './util.service';
import { UserService } from './user.service';
import { StatusTableService } from './status-table.service';
import { StorageService } from './storage.service';
import { SecurityService } from './security.service';
import { NetWorkService } from './network.service';
import { ImageService } from './images.service';
import { EnvironmentService } from './environment.service';
import { FingerprintAIO } from '@ionic-native/fingerprint-aio';
import { IonicStorageModule } from '@ionic/storage';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { AnalyticsService } from '@ons/ons-mobile-analytics';
import { BaseService } from './base.service';
import { MobileLogoutButtonComponent } from './mobile-logout.component';
import { RecaptchaModule } from 'ng-recaptcha';


@NgModule({
  imports: [
    CommonModule,
    RecaptchaModule,
    IonicModule,
    IonicStorageModule.forRoot(),
    HttpClientModule
  ],
  declarations: [MobileLoginComponent, LoginPage, MobileLogoutButtonComponent],
  exports: [MobileLoginComponent, LoginPage, MobileLogoutButtonComponent],
  providers: [],
  entryComponents: [LoginPage, MobileLogoutButtonComponent]

})
export class OnsPackage {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: OnsPackage,
      providers: [AnalyticsService,
        EnvironmentService,
        LoginService,
        ErrorService,
        TokenService,
        UtilService,
        UserService,
        StatusTableService,
        StorageService,
        SecurityService,
        NetWorkService,
        ImageService,
        BaseService,

        // Plugings ↓
        FingerprintAIO]
    };
  }
}
