import { Injectable } from '@angular/core';

/** INTERFACES */
import * as statusTable from './../interfaces/statustable.model';
import * as Device from './../interfaces/Device.model';

/**
 * ESTA CLASSE DEVERÁ SER DESATIVADA NA PROXIMA VERSÃO
 */

@Injectable()
export class StatusTableService {

  _table: statusTable.StatusTable[] = [];

  constructor() {
    this.fillStatusTable();
  }

  private fillStatusTable() {
    this._table.push({ Code: 410, Name: 'Expired - token nulo', Description: 'Token nulo no checagem de expiração' });
    this._table.push({ Code: 420, Name: 'Token nulo', Description: 'Token nulo no checagem de expiração' });
    this._table.push({ Code: 430, Name: 'Token Inválido', Description: '' });
    this._table.push({
      Code: 10, Name: 'Parametro code inválido na gravação do erro',
      Description: 'Somente errorcodes entre 0 e 100 são aceitos'
    });
  }

  getSTatusTable(): statusTable.StatusTable[] {
    return this._table;
  }
}
