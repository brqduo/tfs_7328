
import { Injectable } from '@angular/core';
import { Platform, Config } from 'ionic-angular';
import 'rxjs/add/observable/of';

import { UtilService } from './util.service';

/** NOSSOS PACOTES */
import { AnalyticsService } from '@ons/ons-mobile-analytics';

/** INTERFACES */
import * as network from './../interfaces/network.model';

/** CONFIG */
import { Config as config } from './../environment/config';

declare var navigator: any;
declare var Connection: any;






@Injectable()
export class NetWorkService {

    private states = {};
    private _netStatus = <network.Network>{ mensagem: '', code: -1, connected: false };

    /**
     * Constructor padrão
     * Inicializa os states
     */
    constructor(public utilSrv: UtilService,
        public analyticsSrv: AnalyticsService,
        public configSrv: Config,
        public platform: Platform
    ) {

    }
    /**
     * Atualiza o status da conexão
     */
    public updateNetworkStatus(): Promise<any> {
        return this.platform.ready()
            .then(() => {
                this.setStates();
                if (this.platform.is('cordova')) {
                    let networkState = navigator.connection.type;
                    this._netStatus = this.states[networkState];
                    return this.states[networkState];
                } else {
                    this._netStatus = <network.Network>{ mensagem: config.NetWorkMessages.Connection_ETHERNET, code: 2, connected: true };
                    return this._netStatus;
                }
            })
            .catch((error) => {
                this.analyticsSrv.sendNonFatalCrash('Plataforma não preparada em NetworkService', error);
                this._netStatus = <network.Network>{ mensagem: '', code: -1, connected: false };
                return this._netStatus;
            });
    }

    /**
     * Retorna o status da conexão
     */
    public isNetworkConnected(): network.Network {
        let networkState = navigator.connection.type;
        if ((networkState === undefined) || (networkState === null)) {
            this._netStatus = <network.Network>{ mensagem: config.NetWorkMessages.Connection_ETHERNET, code: 2, connected: true };
            return this._netStatus;
        }
        this._netStatus = this.states[networkState];
        return this._netStatus;
    }

    /**
     * Mensagem padrão de erro de ausência de network
     */
    public NoNetworkMessage() {
        this.analyticsSrv.sendCustomEvent('Problemas de conexão');
        this.utilSrv.alerta(config.NetWorkMessages.SEM_CONEXAO);
    }


    setStates() {
        this.states[Connection.UNKNOWN] = { mensagem: config.NetWorkMessages.Connection_UNKNOW, code: 0, connected: false };
        this.states[Connection.NONE] = { mensagem: config.NetWorkMessages.Connection_NONE, code: 1, connected: false };
        this.states[Connection.ETHERNET] = { mensagem: config.NetWorkMessages.Connection_ETHERNET, code: 2, connected: true };
        this.states[Connection.WIFI] = { mensagem: config.NetWorkMessages.Connection_WIFI, code: 3, connected: true };
        this.states[Connection.CELL_2G] = { mensagem: config.NetWorkMessages.Connection_CELL_2G, code: 4, connected: true };
        this.states[Connection.CELL_3G] = { mensagem: config.NetWorkMessages.Connection_CELL_3G, code: 5, connected: true };
        this.states[Connection.CELL_4G] = { mensagem: config.NetWorkMessages.Connection_CELL_4G, code: 6, connected: true };
        this.states[Connection.CELL] = { mensagem: config.NetWorkMessages.Connection_CELL, code: 7, connected: true };

    }
}
