import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MobileLogoutButtonComponent } from './mobile-logout.component';

describe('MobileLoginComponent', () => {
  let component: MobileLogoutButtonComponent;
  let fixture: ComponentFixture<MobileLogoutButtonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MobileLogoutButtonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MobileLogoutButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
