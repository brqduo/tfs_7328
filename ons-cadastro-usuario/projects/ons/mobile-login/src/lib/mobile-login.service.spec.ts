import { TestBed, inject } from '@angular/core/testing';

import { MobileLoginService } from './mobile-login.service';

describe('MobileLoginService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [MobileLoginService]
    });
  });

  it('should be created', inject([MobileLoginService], (service: MobileLoginService) => {
    expect(service).toBeTruthy();
  }));
});
