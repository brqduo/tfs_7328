import { AnalyticsService } from '@ons/ons-mobile-analytics';
import { Injectable } from '@angular/core';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import { Subject } from 'rxjs/Subject';

import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';

import jwt from 'jwt-decode';
import { JwtHelperService } from '@auth0/angular-jwt';
//import * as jwtClaims from 'jwt-claims';

/** SERVICES */
import { ErrorService } from './error.service';
import { StorageService } from './storage.service';
import { UtilService } from './util.service';
import { EnvironmentService } from './environment.service';
import { NetWorkService } from './network.service';


/** INTERFACES */
import * as Token from './../interfaces/token.model';
import * as Claim from './../interfaces/claim.model';

/** CONFIG */
import { Config as config } from './../environment/config';
import { Observer } from 'rxjs';
import { stringify } from 'querystring';

/**
 * Centraliza as funções relacionadas ao token assim como as informações do token ativo
 * Disponibiliza o evento onTokenChange que dispara sempre que o token é modificado.
 * As informações referentes as CLAIMS também fazem parte desta classe
 * 
 */
@Injectable()
export class TokenService {

  private token: Token.Token;

  public onTokenChange = new Subject<Token.Token>();
  public jwtHelper = new JwtHelperService();
  public clientId: string;

  readonly claims: Claim.Claim[];
  readonly EXPIRED = true;
  readonly NOTEXPIRED = false;

  constructor(public errorSrv: ErrorService
    , public storageSrv: StorageService
    , public utilSrv: UtilService
    , public envSrv: EnvironmentService
    , public http: HttpClient
    , public netSrv: NetWorkService
    , public analyticsSrv: AnalyticsService
  ) {

    this.netSrv.updateNetworkStatus();
    console.log('Executando Constructor');

  }

  /**
   * 
   * @param clientId Informa o clientId para usar com o POP
   */
  setClientId(clientId: string) {
    this.clientId = clientId;
  }

  /**
   * 
   * @param clientId Verifica se o token precisa ser renovado ou não
   */
  doRefresh(clientId?: string) {
    if (!clientId)
      clientId = this.clientId;
    if (this.isExpired()) {
      this.doRenew(clientId);
    }
  }

  /**
   * Renova o token corrente
   * @param clientId Id do usuário do token.Este parâmetro é essencial para o renew
   */
  public doRenew(clientId?: string): Observable<Object> {
    if (!clientId)
      clientId = this.clientId;
    let _headers: HttpHeaders;
    if (!this.getToken() || !this.getToken().refresh_token) {
      console.log('Não tinha o refresh_token armazenado');
      return Observable.of(this.empty()); // false);
    }
    _headers = new HttpHeaders();
    _headers = _headers.append('Content-Type', 'application/x-www-form-urlencoded');
    _headers = _headers.append('Accept', 'application/json');
    const body = 'grant_type=refresh_token&client_id=' + clientId + '&refresh_token=' +
      encodeURIComponent(this.getToken().refresh_token.trim());
    if (this.netSrv.isNetworkConnected().connected) {
      return this.http.post(this.envSrv.current.value + '/token', body, { headers: _headers })
        .map(
          (data) => {
            this.setToken(data);
            this.storageSrv.Gravar(config.Keys.tokenUser, data);
            this.onTokenChange.next(<Token.Token>data);
            return data;
          },
          error => {
            this.analyticsSrv.sendNonFatalCrash('refresh - ' + config.mensagemGenerica.TOKEN_ERROR_REFRESH, error);
            return Observable.of(this.empty()); // false);
          })
        .catch((error) => {
          this.analyticsSrv.sendNonFatalCrash('refresh - ' + config.mensagemGenerica.TOKEN_ERROR_REFRESH, error);
          return Observable.of(this.empty());
        });
    } else {
      this.analyticsSrv.sendNonFatalCrash(config.NetWorkMessages.SEM_CONEXAO, '');
      return Observable.of(this.empty());
    }
  }
  /**
   * Renova o token corrente retordo erro em caso de falha tecnica
   * @param clientId Id do usuário do token.Este parâmetro é essencial para o renew
   */
  public doRenew2(clientId?: string): Observable<Object> {
    return Observable.create(

      (observer: Observer<any>) => {
        let _headers: HttpHeaders;
        if (!clientId)
          clientId = this.clientId;
        if (!this.getToken() || !this.getToken().refresh_token) {
          console.log('Não tinha o refresh_token armazenado');
          observer.next(this.empty());
        }
        _headers = new HttpHeaders();
        _headers = _headers.append('Content-Type', 'application/x-www-form-urlencoded');
        _headers = _headers.append('Accept', 'application/json');
        const body = 'grant_type=refresh_token&client_id=' + clientId + '&refresh_token=' +
          encodeURIComponent(this.getToken().refresh_token.trim());
        if (this.netSrv.isNetworkConnected().connected) {
          this.http.post(this.envSrv.current.value + '/token', body, { headers: _headers })
            .subscribe(
              (data) => {
                this.setToken(data);
                this.storageSrv.Gravar(config.Keys.tokenUser, data);
                this.onTokenChange.next(<Token.Token>data);
                observer.next(data);
              },
              error => {
                this.analyticsSrv.sendNonFatalCrash('refresh - ' + config.mensagemGenerica.TOKEN_ERROR_REFRESH, error);
                if (error instanceof HttpErrorResponse) {
                  if (error.status >= 400 && error.status < 500) {
                    observer.next(this.empty());
                  }
                  else {
                    observer.error('Erro ao renovar o token: ' + JSON.stringify(error));
                  }

                } else {
                  observer.error('Erro ao renovar o token: ' + JSON.stringify(error));
                }
              }
            );
        } else {
          this.analyticsSrv.sendNonFatalCrash(config.NetWorkMessages.SEM_CONEXAO, '');
          observer.error('Não tem acesso de rede');
        }
      })
  }

  /**
   * Retorna o token decodificado
   * @param tk Token opcional. Caso não seja informado o sistema utikliza o token ativo
   */
  getDecoded(tk?): Object {
    if ((tk == null) || (tk === undefined)) {
      tk = this.getToken();
    }
    if (this.isNull(tk)) {
      try {

        return jwt(tk.access_token);
      } catch (error) {
        this.errorSrv.add(config.Error.TOKEN_INVALID, config.mensagemGenerica.TOKEN_ERROR_DECODE + tk.access_token);
        return <Token.Token>{
          access_token: '',
          token_type: '',
          expires_in: -1,
          refresh_token: ''
        };
      }
    } else {
      return null;
    }
  }

  /**
   * Retorna os clais em forma de array of Claim
   * @param tk Token opcional. Caso não seja informado o sistema utikliza o token ativo
   */
  getClaims(tk?: Token.Token): Claim.Claim[] {
    let Claims: Array<Claim.Claim> = [];
    if ((tk === null) || (tk === undefined)) {
      tk = this.getToken()
    }
    if (this.isValid(tk)) {
      const ArJwt: any = Object.entries(this.getDecoded());
      for (let i = 0; i < ArJwt.length; i++) {
        const element = ArJwt[i];
        if (element[0].match(/claims/g)) {
          let objClaims = <Claim.Claim>{ name: element[0], value: element[1] };
          Claims.push(objClaims);
        }
      }
      return Claims;
    } else {
      return null;
      // this.errorSrv.add(420, 'Tentantiva de getClaims um token inválido')
    }
  }


  /**
   * Retorna o token ativo
   */
  getToken(): Token.Token {
    if ((this.token === null) || (this.token === undefined)) {
      return this.empty();
    } else {
      return this.token;
    }
  }

  /**
   * Retorna o token armazenado no device
   */
  getTokenFromStorage(): Promise<any> {
    return this.storageSrv.recuperar(config.Keys.tokenUser)
      .then(data => {
        //this.token = <Token.Token>data;
        this.setToken(data);
        return this.getToken();
      }).catch((err) => {
        return this.empty();
      });
  }

  public generateTokenFromSTring(tk: string): Token.Token {
    let tok = jwt(tk);
    return <Token.Token>{ access_token: tk, token_type: 'Bearer', expires_in: tok.exp, refresh_token: '' };
  }

  /**
   * Seta o token no ambiente
   * @param tk Token opcional. Caso não seja informado o sistema utikliza o token ativo
   */
  setToken(tk) {
    if (this.isValid(tk)) {
      try {
        this.token = <Token.Token>tk;
        this.onTokenChange.next(<Token.Token>tk);

      } catch (error) {
        this.errorSrv.add(config.Error.TOKEN_SET_ERROR, config.mensagemGenerica.TOKEN_ERROR_SET);
      }
    } else {
      this.errorSrv.add(config.Error.TOKEN_INVALID, config.mensagemGenerica.TOKEN_ERROR_SET);
    }
  }

  /**
   * Verifica se o token esta expirado
   * @param token Token opcional. Caso não seja informado o sistema utikliza o token ativo
   */
  isExpired(token?: Token.Token) {
    if ((token === null) || (token === undefined)) {
      token = this.getToken();
    }
    if (token !== null) {
      return this.jwtHelper.isTokenExpired(token.access_token);
    } else {
      this.errorSrv.add(config.Error.TOKEN_NULL, config.mensagemGenerica.TOKEN_ERROR_NULL_OR_EXPIRED);
      return true;
    }
  }

  /**
   * Verifica se o token é válido
   * @param tk Token opcional. Caso não seja informado o sistema utikliza o token ativo
   */
  isValid(tk?: Token.Token): boolean {
    if (!this.isNull(tk)) {
      this.errorSrv.add(config.Error.TOKEN_INVALID, config.mensagemGenerica.TOKEN_ERROR_NULL_OR_INALID);
      return false;
    } else {
      if ((tk.access_token !== undefined) && (tk.access_token !== null) && (tk.access_token !== '')) {
        try {
          this.getDecoded(tk);
          return true;
        } catch (error) {
          return false;
        }
      } else {
        this.errorSrv.add(config.Error.TOKEN_INVALID, config.mensagemGenerica.TOKEN_ERROR_NULL_OR_INALID);
        return false;
      }
    }
  }

  /**
   * Verifica se o token é nulo
   *
   * @param tk - Token a ser validado. Caso o mesmo não seja informado o sistema utiliza o TOKEN interno do app
   *
   */
  isNull(tk?: Token.Token): boolean {
    if ((tk === null) || (tk === undefined)) {
      tk = this.token;
    }
    try {
      return ((tk !== null) && (tk !== undefined));
    } catch (error) {
      this.errorSrv.add(config.Error.TOKEN_NULL_ERROR, 'Erro no processo null ');
      return false;
    }

  }

  /**
   * Recupera uma claim do token baseado no nome da mesma. Caso a Claim não exista ele retorna um string em branco ''.
   * @param name Nome da claim a ser recuperada
   */
  getDecodedByName(name: string): any {
    const ArJwt: any = Object.entries(this.getDecoded());
    let retorno: any = '';
    for (let i = 0; i < ArJwt.length; i++) {
      const element = ArJwt[i];
      if (element[0] === name) {
        retorno = element[1];
      }
    }
    return retorno;
    // this.errorSrv.add(420, 'Tentantiva de getClaims um token inválido')
  }

  /**
   * Retorna um objeto Token vazio
   */
  public empty(): Token.Token {
    return <Token.Token>{ access_token: '', token_type: '', expires_in: -1, refresh_token: '' };
  }


  public getUserNameFromToken(): string {
    if ((this.getToken() !== null) && (this.getToken() !== undefined)) {
      return this.getDecodedByName(config.Claim_UserId);
    }
  }

  private generateToken(t: Token.Token): Token.Token {
    return <Token.Token>{ access_token: '', token_type: '', expires_in: -1, refresh_token: '' };
  }

  //private tokenN
}
