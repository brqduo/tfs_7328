import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { LoginService } from './login.service';

@Component({
  selector: 'mobile-logout-button',
  template: `
    <ion-buttons end>
      <button ion-button icon-only color="primary_20" (click)="openLogoff($event)">
        <ion-icon name="log-out"></ion-icon>
      </button>
    </ion-buttons>
  `,
  styles: []
})
export class MobileLogoutButtonComponent implements OnInit {

  @Input() page: string;
  @Input() showBack: boolean = false;
  @Output() callbackVoltar: EventEmitter<any> = new EventEmitter();
  constructor(public loginService: LoginService) {
  }

  ngOnInit() {

  }
  ngOnDestroy() {

  }
  openLogoff(ev) {

    this.loginService.showLogoutOptions(this.page, this.showBack, this.callbackVoltar);
  }

}
