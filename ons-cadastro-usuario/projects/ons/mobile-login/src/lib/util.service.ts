import { Subject } from 'rxjs/Subject';
import 'rxjs/add/observable/of';
import { Injectable } from '@angular/core';
import { ToastController } from 'ionic-angular';
import { AlertController } from 'ionic-angular';
import { HttpHeaders, HttpClient } from '@angular/common/http';

/* import { Storage } from "@ionic/storage"; */
import { AnalyticsService } from '@ons/ons-mobile-analytics';

import { Platform, LoadingController, } from 'ionic-angular';

// import { AuthProvider } from '../providers/auth/auth';

import 'rxjs/add/operator/map';

import { EnvironmentService } from './environment.service';
import { Observable, Observer } from 'rxjs';


@Injectable()
export class UtilService {

    posMsg: any = ['bottom', 'top', 'middle'];
    filtroAtivo = '';
    EmAnaliseErro = false;

    config: any = {};
    loading: any;

    refreshContatos = new Subject();
    ObservableFecharGerencias = new Subject<any>();
    VersaoAtual: Subject<string> = new Subject<string>();
    isAtual: Subject<boolean> = new Subject<boolean>();

    public httpOptions = {
        headers: new HttpHeaders({
            'content-type': 'application/x-www-form-urlencoded',
            'accept': 'application/json'
        })
    };

    constructor(public toastSrv: ToastController
        , public alertCtrl: AlertController
        , private loadingCrl: LoadingController
        , public platform: Platform
        , /* public storage: Storage */
        private analyticSrv: AnalyticsService
        , private http: HttpClient
        , private enviromentService: EnvironmentService
        // , public authSrv: AuthProvider
    ) {
        this.config.banco = {};
        this.config.banco.url = 'keychain.br.org.ons';
        this.config.textKeys = {};
        this.config.textKeys.chaveLogin = 'automaticLogin';
        this.config.textKeys.jwtTokenName = 'jwt_token';
        this.config.textKeys.ForceLogin = 'ForceLogin';
    }

    alerta(mensagem,
        delay?: number,
        pos?: string,
        titulo?: string,
        btnClose?: boolean) {
        delay = (((delay === undefined) || (delay === null)) ? 3000 : delay);
        pos = ((this.posMsg.indexOf(pos) === -1) ? 'top' : pos);
        btnClose = (((btnClose === undefined) || (btnClose === null)) ? false : btnClose);
        mensagem = (((titulo === undefined) || (titulo === null)) ? mensagem : titulo + '\n\n' + mensagem);
        const toast = this.toastSrv.create(
            {
                message: mensagem,
                duration: delay,
                position: pos,
                showCloseButton: btnClose,
                closeButtonText: 'Fechar'
            });
        toast.present();
    }


    ErrorAlerta(mensagem, delay?: number, pos?: string, titulo?: string, btnClose?: boolean) {
        if (this.EmAnaliseErro) {
            delay = (((delay === undefined) || (delay === null)) ? 3000 : delay);
            pos = ((this.posMsg.indexOf(pos) === -1) ? 'middle' : pos);
            btnClose = (((btnClose === undefined) || (btnClose === null)) ? false : btnClose);
            mensagem = (((titulo === undefined) || (titulo === null)) ? mensagem : titulo + '\n\n' + mensagem);

            const toast = this.toastSrv.create(
                {
                    message: mensagem,
                    duration: delay,
                    position: pos,
                    showCloseButton: btnClose,
                    closeButtonText: 'Fechar'
                });
            toast.present();
        }
    }

    ativarBloqueioMsg(msg?: string) {
        const mensagem = (((msg === undefined) || (msg == null)) ? 'logando...' : msg);
        this.loading = this.loadingCrl.create({
            content: mensagem
        });
        this.loading.present();
    }

    desativarBloqueioMsg() {
        try {
            this.loading.dismiss();
        } catch (error) {
            console.log('dismiss invaid');
        }
    }
     /**
         * Função obsoleta, foi trocada pela checkVersion
        
         */
    isVersaoAtual(nomeApp, numeroVersao) {
        this.VersaoAtual.next(numeroVersao);
        let headers = new HttpHeaders();
        let promise = new Promise((resolve, reject) => {
            if (!this.enviromentService.config || !this.enviromentService.config.urlRemoteConfig) {
                reject('Não foi configurado o config.urlRemoteConfig');
            }
            let apiURL = this.enviromentService.config.urlRemoteConfig + "api/firebase/isVersaoAtual?nomeApp=" + nomeApp + "&numeroVersao=" + numeroVersao;
            this.http.get(apiURL, { headers: headers })
                .subscribe((data: any) => {
                    resolve(data);
                    console.log(data);
                    if (!data.resposta) {
                        this.isAtual.next(data);
                    }
                }, (err) => {
                    reject(err);
                });

        });

        return promise;
    }

    /**
         * Retorna true caso a versao esta ok. False indica que vai atualizar
         * O callback deve ser implementado para acionar a loja de acordo com a url retornada
         
         */
    checkVersion(nomeApp, numeroVersao, callbackUpdateVersion: Function): Observable<any> {

        return Observable.create(

            (observer: Observer<any>) => {
                if (!this.enviromentService.config || !this.enviromentService.config.urlRemoteConfig) {
                    observer.error('Não foi configurado o config.urlRemoteConfig');
                }
                let headers = new HttpHeaders();
                let apiURL = this.enviromentService.config.urlRemoteConfig + "api/firebase/isVersaoAtual?nomeApp=" + nomeApp + "&numeroVersao=" + numeroVersao;
                this.http.get(apiURL, { headers: headers })
                    .subscribe(
                        (resp: any) => {
                            if (resp.EstaAtualizado) { //não precisa atualizar
                                observer.next(true);
                            } else {
                                if (resp.AtualizacaoObrigatoria) {//tem que atualizar agora

                                    const alert = this.alertCtrl.create({


                                        message: resp.mensagem,
                                        buttons: ['OK']
                                    });
                                    alert.onDidDismiss(() => {
                                        callbackUpdateVersion(resp);
                                        observer.next(false);
                                    });
                                    alert.present();


                                } else { //pode atualizar mais tartde
                                    let alert = this.alertCtrl.create({
                                        message: resp.mensagem + " Deseja fazer o upgrade agora?",
                                        buttons: [
                                            {
                                                text: 'Sim',
                                                handler: () => {
                                                    callbackUpdateVersion(resp);
                                                    observer.next(false);
                                                }
                                            }, {
                                                text: 'Não',
                                                handler: () => {
                                                    observer.next(true);
                                                }
                                            }
                                        ]
                                    });

                                    alert.present();
                                }
                            }

                            console.log(resp);
                        },
                        error => {
                            observer.next(true);
                        });
            }
        )
    }

    /**
     * Faz o encerramento da aplicação.
     * Somente nãoé exigida a confirmação se o desenvolvedor for explícito na chamada
     * Por padrão o parametro dera true caso o usuário não informe
     *
     * pa@ram {boolean} [confirm] <= mudar o "@" para o começo da sentenã caso queira usa-la
     *
     * @memberOf UtilService
     */
    exitApp(confirm?: boolean) {
        confirm = ((confirm === undefined || confirm == null) ? true : confirm);
        if (confirm) {
            this.alertCtrl.create({
                title: 'Deseja mesmo sair?',
                message: 'Você tem certeza que deseja mesmo se deslogar do aplicativo?',
                buttons: [
                    {
                        text: 'Não'
                    },
                    {
                        text: 'Sim',
                        handler: () => {
                            this.analyticSrv.sendNonFatalCrash('Sair', {});
                            this.platform.exitApp();
                        }
                    }
                ]
            });
        } else {
            this.analyticSrv.sendNonFatalCrash('Sair Sem confirmação', {});
            this.platform.exitApp();
        }
    }
    getFormData(object) {
        const formData = new FormData();
        Object.keys(object).forEach(key => formData.append(key, object[key]));
        return formData;
    }
    padLeft(text: string, padChar: string, size: number): string {
        return (String(padChar).repeat(size) + text).substr((size * -1), size);
    }
    IsStringValid(stringToCheck: string) {
        if (stringToCheck.length == 0 ||
            stringToCheck == "" ||
            stringToCheck == undefined ||
            stringToCheck == null) {
            return false;
        }

        return true;
    }
}
