import { Storage } from '@ionic/storage';
import { Injectable } from '@angular/core';

import { Subject } from 'rxjs/Subject';
import 'rxjs/add/observable/of';

/** INTERFACES */
import * as Claim from './../interfaces/claim.model';
import * as Token from './../interfaces/token.model';


/** SERVICES */
import { ErrorService } from './error.service';
import { Config } from './../environment/config';
import { TokenService } from './token.service';
import { AnalyticsService } from '@ons/ons-mobile-analytics';
import { LoginService } from './login.service';
import { UtilService } from './util.service';

/** CONFIG */
import { Config as config } from './../environment/config';



@Injectable()
/**
 * Classe responsavel por todas as operações no storage.
 */
export class SecurityService {

    public onStorageError = new Subject<any>();
    public onStorageOk = new Subject<any>();

    private _scopeType: string;
    private _scope: string;
    private _groups: string[] = [];
    private _roles: string[] = [];
    private _scopeRoles: string[] = [];
    private _operations: string[] = [];

    get Groups(): string[] {
        return this._groups;
    }

    retornoTotal() {
        const res = {
            '___groups': this._groups,
            '___roles': this._roles,
            '___scopeRoles': this._scopeRoles,
            '___scopeType': this._scopeType,
            '___scope': this._scope,
            '___operations': this._operations
        };
        return res;
    }

    get Operations(): string[] {
        return this._operations
    }

    get Roles(): string[] {
        return this._roles
    }

    get ScopeRoles(): string[] {
        return this._scopeRoles;
    }

    get Scope(): string {
        return this._scope;
    }

    get ScopeType(): string {
        return this._scopeType;
    }

    get TypeScope(): string {
        return this._scope + '/' + this.ScopeType;
    }

    constructor(public storage: Storage
        , public errorSrv: ErrorService
        , public tokenSrv: TokenService
        , public analiticSrv: AnalyticsService
        , public utilSrv: UtilService
        , public loginSrv: LoginService) {

            this.loginSrv.onConectedChange.subscribe(data =>{
                this.init();
                console.log(data);
            });
    }

    init() {
        this.fillOperations();
        this.fillGroups();
        this.fillRoles();
        this.fillScopeRole();

        console.log('Operation by Scope not Allowed Type Error', this.isOperationAllowedByScope("ONS","ONE","Aprovar Recebimento Físico"));
        console.log('Operation by Scope not Allowed Scope Error', this.isOperationAllowedByScope("ONE","ONS","Aprovar Recebimento Físico"));
        console.log('Operation by Scope not Allowed Operation Error', this.isOperationAllowedByScope("ONS","ONE","Aprovar Recebimento Externo"));
        console.log('Operation by Scope Allowed', this.isOperationAllowedByScope("ONS","ONS","Aprovar Recebimento Físico"));

        console.log('Role by Scope not Allowed Type Error', this.isRoleAllowedByScope("ONS","ONE","Aprovador Técnico"));
        console.log('Role by Scope not Allowed Scope Error', this.isRoleAllowedByScope("ONE","ONS","Aprovador Técnico"));
        console.log('Role by Scope not Allowed Operation Error', this.isRoleAllowedByScope("ONS","ONE","Aprovador Técnico"));
        console.log('Role by Scope Allowed', this.isRoleAllowedByScope("ONS","ONS","Aprovador Técnico"));

        //console.log('Claims: ', this.tokenSrv.getClaims());
    }

    private fillGroups() {
        this._groups = [];

        try {
            this.tokenSrv.getClaims().forEach((element: any) => {
                if (element.name === config.ClaimTags.Group) {
                    element.value.forEach((g: string) => {
                        this._groups.push(g);
                    });
                }
            });
        }
        catch (error) {
            this.errorSrv.add(config.Error.SECURITY_CLAIM_ERROR, config.mensagemGenerica.SECURITY_CLAIM_ERROR);
            this.analiticSrv.sendNonFatalCrash(config.mensagemGenerica.SECURITY_CLAIM_ERROR, error);
        }

        console.log('Groups: ', this._groups);
    }


    private fillOperations() {
        this._operations = [];

        try {
            this.tokenSrv.getClaims().forEach((element: any) => {
                if (element.name === config.ClaimTags.Operation) {
                    element.value.forEach((g: string) => {
                        this._operations.push(g);
                    });
                }
            });
        }
        catch (error) {
            this.errorSrv.add(config.Error.SECURITY_CLAIM_ERROR, config.mensagemGenerica.SECURITY_CLAIM_ERROR);
            this.analiticSrv.sendNonFatalCrash(config.mensagemGenerica.SECURITY_CLAIM_ERROR, error);
        }

        console.log('Operações: ', this._operations);
    }


    private fillRoles() {
        this._roles = [];

        try {
            this.tokenSrv.getClaims().forEach((element: any) => {
                if (element.name === config.ClaimTags.ScopeRole) {
                    element.value.forEach((g: string) => {
                        this._roles.push(g);
                    });
                }
            });
        } catch (error) {
            this.errorSrv.add(config.Error.SECURITY_CLAIM_ERROR, config.mensagemGenerica.SECURITY_CLAIM_ERROR);
            this.analiticSrv.sendNonFatalCrash(config.mensagemGenerica.SECURITY_CLAIM_ERROR, error);
        }


        console.log('Roles: ', this._roles);
    }

    private fillScopeRole() {
        this._scopeRoles = [];

        try {
            this.tokenSrv.getClaims().forEach((element: any) => {
                if (element.name === config.ClaimTags.ScopeRole) {
                    element.value.forEach((g: string) => {
                        this._scopeRoles.push(g);
                    });
                }
            });
        } catch (error) {
            this.errorSrv.add(config.Error.SECURITY_CLAIM_ERROR, config.mensagemGenerica.SECURITY_CLAIM_ERROR);
            this.analiticSrv.sendNonFatalCrash(config.mensagemGenerica.SECURITY_CLAIM_ERROR, error);
        }

        console.log('ScopeRoles: ', this._scopeRoles);
    }

    private getOperations() {
        console.log('Claims ', this.tokenSrv.getClaims())
        this.tokenSrv.getClaims().forEach((element: Claim.Claim) => {
            console.log('ELEMENT ', element);
        });
    }

    // private getRoles() {
    //     console.log('Claims ', this.tokenSrv.getClaims());
    //     this.tokenSrv.getClaims().forEach((element: Claim.Claim) => {
    //         console.log('ELEMENT ', element);
    //     });
    // }



    public setScopeType(scope: string, tipo: string) {
        this._scope = scope;
        this._scopeType = tipo;
    }


    // public isOperationAllowed(operation: string): boolean {
    //     console.log(this.getOperations());

    //     return false;
    // }

    public isGlobalAccess(operation: string) {
        
        let Operations = [];

        try 
        {
            if (!this.utilSrv.IsStringValid(operation)) {

                this.errorSrv.add(config.Error.SECURITY_GLOBAL_ACCESS_ERROR, config.mensagemGenerica.SECURITY_GLOBAL_ACCESS_ERROR);
                this.analiticSrv.sendNonFatalCrash(config.mensagemGenerica.SECURITY_GLOBAL_ACCESS_ERROR, operation);
                return false;
            }
    
            Operations = this.getScopeOperations();
            console.log('Global Operations: ', Operations);
    
            for (let index = 0; index < Operations.length; index++) {
                const ScopeOperation = Operations[index];
    
                if (ScopeOperation.Operation.toLowerCase().trim() === operation.toLowerCase().trim()) {
                    return true;
                }
                    
            }
            return false; 

        } catch (error) 
        {
            this.errorSrv.add(config.Error.SECURITY_GLOBAL_ACCESS_ERROR, config.mensagemGenerica.SECURITY_GLOBAL_ACCESS_ERROR);
            this.analiticSrv.sendNonFatalCrash(config.mensagemGenerica.SECURITY_GLOBAL_ACCESS_ERROR, error);
        }
       

    }

    public isGlobalAccessByScope(scope: string, operation: string) {

        let ScopeOperations = [];

        scope = this.returnDefaultScope(scope);

        try 
        {
            if (!this.utilSrv.IsStringValid(scope) || !this.utilSrv.IsStringValid(operation)) {

                let err = {
                    scope,
                    operation
                }

                this.errorSrv.add(config.Error.SECURITY_GLOBAL_ACCESS_SCOPE_ERROR, config.mensagemGenerica.SECURITY_GLOBAL_ACCESS_SCOPE_ERROR);
                this.analiticSrv.sendNonFatalCrash(config.mensagemGenerica.SECURITY_GLOBAL_ACCESS_SCOPE_ERROR, err);
                return false;
            }
    
            ScopeOperations = this.getScopeOperations();
            console.log('Global Scope Operations: ', ScopeOperations);
    
            for (let index = 0; index < ScopeOperations.length; index++) {
                const ScopeOperation = ScopeOperations[index];
    
                if (ScopeOperation.Scope.toLowerCase().trim() === scope.toLowerCase().trim() &&
                    ScopeOperation.Operation.toLowerCase().trim() === operation.toLowerCase().trim()) {
                    return true;
                }
                    
            }
            return false; 

        } catch (error) 
        {
            this.errorSrv.add(config.Error.SECURITY_GLOBAL_ACCESS_SCOPE_ERROR, config.mensagemGenerica.SECURITY_GLOBAL_ACCESS_SCOPE_ERROR);
            this.analiticSrv.sendNonFatalCrash(config.mensagemGenerica.SECURITY_GLOBAL_ACCESS_SCOPE_ERROR, error);
        }
    }

    public IsAllowed(type: string, valor: string)
    {

        type = this.returnDefaultType(type);

        if (!this.utilSrv.IsStringValid(valor) || !this.utilSrv.IsStringValid(type)) {

            let err = {
                type,
                valor
            }

            this.errorSrv.add(config.Error.SECURITY_ALLOWED_ERROR, config.mensagemGenerica.SECURITY_ALLOWED_ERROR);
            this.analiticSrv.sendNonFatalCrash(config.mensagemGenerica.SECURITY_ALLOWED_ERROR, err);
            return false;
        }

        try 
        {
            let Claims = this.tokenSrv.getClaims();

            console.log('Claims: ', Claims);

            for (let i = 0; i < Claims.length; i++) {

                const claim = Claims[i];

                if (claim.name.toLowerCase().trim() === type.toLowerCase().trim()) {

                    for (let index = 0; index < claim.value.length; index++) {

                        const op = claim.value[index].toLowerCase().trim();

                        if (op === valor.toLowerCase().trim()) {
                            return true; 
                        }
                    }
                }
            }
            return false;

        } catch (error) {
            this.errorSrv.add(config.Error.SECURITY_CLAIM_ERROR, config.mensagemGenerica.SECURITY_CLAIM_ERROR);
            this.analiticSrv.sendNonFatalCrash(config.mensagemGenerica.SECURITY_CLAIM_ERROR, error);
        }

    }

    public isRoleAllowed(Role: string) {
        try {

            let Roles = [];

            if (!this.utilSrv.IsStringValid(Role)) {
    
                this.errorSrv.add(config.Error.SECURITY_ROLE_ALLOWED_ERROR, config.mensagemGenerica.SECURITY_ROLE_ALLOWED_ERROR);
                this.analiticSrv.sendNonFatalCrash(config.mensagemGenerica.SECURITY_ROLE_ALLOWED_ERROR, Role);
                return false;
            }

            Roles = this.getRoles();
            console.log('Roles: ', Roles);

            for (let index = 0; index < Roles.length; index++) {
                const role = Roles[index];

                if (role === Role) {
                    return true;
                }
                
            }

            return false;
        } catch (error) {
            this.errorSrv.add(config.Error.SECURITY_ROLE_ALLOWED_ERROR, config.mensagemGenerica.SECURITY_ROLE_ALLOWED_ERROR);
            this.analiticSrv.sendNonFatalCrash(config.mensagemGenerica.SECURITY_ROLE_ALLOWED_ERROR, error);
        }
    }

    public isOperationAllowedByScope(scope: string, type: string, operation: string) {
        try 
        {

            let ScopeOperations = [];

            scope = this.returnDefaultScope(scope);
            type = this.returnDefaultScope(type);

            if (!this.utilSrv.IsStringValid(scope) ||
                !this.utilSrv.IsStringValid(type) ||
                !this.utilSrv.IsStringValid(operation)) {

                    let err = {
                        scope: scope,
                        type: type,
                        operation: operation
                    }
    
                this.errorSrv.add(config.Error.SECURITY_OPERATION_ALLOWED_ERROR, config.mensagemGenerica.SECURITY_OPERATION_ALLOWED_ERROR);
                this.analiticSrv.sendNonFatalCrash(config.mensagemGenerica.SECURITY_OPERATION_ALLOWED_ERROR, err);
                return false;
            }

            ScopeOperations = this.getScopeOperations();
            console.log('Operations: ', ScopeOperations);

            for (let index = 0; index < ScopeOperations.length; index++) {
                const ScopeOperation = ScopeOperations[index];

                if (ScopeOperation.Scope.toLowerCase().trim() === scope.toLowerCase().trim() &&
                    ScopeOperation.Type.toLowerCase().trim() === type.toLowerCase().trim() &&
                    ScopeOperation.Operation.toLowerCase().trim() === operation.toLowerCase().trim()) {
                    return true;
                }
                
            }

            return false;
        } catch (error) {
            this.errorSrv.add(config.Error.SECURITY_OPERATION_ALLOWED_ERROR, config.mensagemGenerica.SECURITY_OPERATION_ALLOWED_ERROR);
            this.analiticSrv.sendNonFatalCrash(config.mensagemGenerica.SECURITY_OPERATION_ALLOWED_ERROR, error);
        }
    }

    public isRoleAllowedByScope(scope: string, type: string, role: string) {
        try 
        {

            let ScopeRoles = [];

            scope = this.returnDefaultScope(scope);
            type = this.returnDefaultScope(type);

            if (!this.utilSrv.IsStringValid(scope) ||
                !this.utilSrv.IsStringValid(type) ||
                !this.utilSrv.IsStringValid(role)) {

                    let err = {
                        scope: scope,
                        type: type,
                        role: role
                    }
    
                this.errorSrv.add(config.Error.SECURITY_ROLE_ALLOWED_ERROR, config.mensagemGenerica.SECURITY_ROLE_ALLOWED_ERROR);
                this.analiticSrv.sendNonFatalCrash(config.mensagemGenerica.SECURITY_ROLE_ALLOWED_ERROR, err);
                return false;
            }

            ScopeRoles = this.getScopeRoles();
            console.log('Roles: ', ScopeRoles);

            for (let index = 0; index < ScopeRoles.length; index++) {
                const ScopeRole = ScopeRoles[index];

                if (ScopeRole.Scope.toLowerCase().trim() === scope.toLowerCase().trim() &&
                    ScopeRole.Type.toLowerCase().trim() === type.toLowerCase().trim() &&
                    ScopeRole.Role.toLowerCase().trim() === role.toLowerCase().trim()) {
                    return true;
                }
                
            }

            return false;
        } catch (error) {
            this.errorSrv.add(config.Error.SECURITY_ROLE_ALLOWED_ERROR, config.mensagemGenerica.SECURITY_ROLE_ALLOWED_ERROR);
            this.analiticSrv.sendNonFatalCrash(config.mensagemGenerica.SECURITY_ROLE_ALLOWED_ERROR, error);
        }
    }

    public isGroupAllowedByScope(scope: string, type: string, group: string) {
        try 
        {

            let Groups = [];
            let Scopes =[];

            scope = this.returnDefaultScope(scope);
            type = this.returnDefaultScope(type);

            if (!this.utilSrv.IsStringValid(scope) ||
                !this.utilSrv.IsStringValid(type) ||
                !this.utilSrv.IsStringValid(group)) {

                    let err = {
                        scope: scope,
                        type: type,
                        group: group
                    }
    
                this.errorSrv.add(config.Error.SECURITY_GROUP_ALLOWED_ERROR, config.mensagemGenerica.SECURITY_GROUP_ALLOWED_ERROR);
                this.analiticSrv.sendNonFatalCrash(config.mensagemGenerica.SECURITY_GROUP_ALLOWED_ERROR, err);
                return false;
            }

            Scopes = this.getScopes();
            Groups = this.getGroups();
            console.log('Groups: ', Groups);

            for (let index = 0; index < Scopes.length; index++) {
                const Scope = Scopes[index];

                for (let index = 0; index < Groups.length; index++) {
                    const Group = Groups[index];

                    if (Scope.Scope.toLowerCase().trim() === scope.toLowerCase().trim() &&
                        Scope.Type.toLowerCase().trim() === type.toLowerCase().trim() &&
                        Group.toLowerCase().trim() === group.toLowerCase().trim()) {
                        return true;
                    }
                    
                }
                
            }

            return false;
        } catch (error) {
            this.errorSrv.add(config.Error.SECURITY_GROUP_ALLOWED_ERROR, config.mensagemGenerica.SECURITY_GROUP_ALLOWED_ERROR);
            this.analiticSrv.sendNonFatalCrash(config.mensagemGenerica.SECURITY_GROUP_ALLOWED_ERROR, error);
        }
    }

      /**
   * Retorna a role em forma de array
   * @param tk? Token opcional. Caso não seja informado o sistema utikliza o token ativo
   */
    private getRoles(tk?: Token.Token): string[] {
        let Roles =  [];
        
        if ((tk === null) || (tk === undefined)) 
        {
            tk = this.tokenSrv.getToken()
        }

        if (this.tokenSrv.isValid(tk)) 
        {
            const ArJwt: any = Object.entries(this.tokenSrv.getDecoded());

            for (let i = 0; i < ArJwt.length; i++) {
                const element = ArJwt[i];
                if (element[0].match(/^role$/)) {
                    if(element[1] instanceof Array)
                    {
                        element[1].forEach(roleArr => {
                            Roles.push(roleArr);
                        });
                    }
                    else
                    {
                        Roles.push(element[1]);
                    }
                }
            }
            return Roles;
        } 
        else 
        {
            return null;
            // this.errorSrv.add(420, 'Tentantiva de getClaims um token inválido')
        }
    }

    private getGroups(tk?: Token.Token): string[] {
        let Groups =  [];
        
        if ((tk === null) || (tk === undefined)) 
        {
            tk = this.tokenSrv.getToken()
        }

        if (this.tokenSrv.isValid(tk)) 
        {
            const ArJwt: any = Object.entries(this.tokenSrv.getDecoded());

            for (let i = 0; i < ArJwt.length; i++) {
                const element = ArJwt[i];
                if (element[0].match(/group$/)) {
                    if(element[1] instanceof Array)
                    {
                        element[1].forEach(roleArr => {
                            Groups.push(roleArr);
                        });
                    }
                    else
                    {
                        Groups.push(element[1]);
                    }
                }
            }
            return Groups;
        } 
        else 
        {
            return null;
            // this.errorSrv.add(420, 'Tentantiva de getClaims um token inválido')
        }
    }

    private getScopeOperations(tk?: Token.Token): string[] {

        let ScopeOperations =  [];

        if ((tk === null) || (tk === undefined)) 
        {
            tk = this.tokenSrv.getToken()
        }

        if (this.tokenSrv.isValid(tk)) 
        {
            const ArJwt: any = Object.entries(this.tokenSrv.getDecoded());

            for (let i = 0; i < ArJwt.length; i++) {
                let element = ArJwt[i];
                if (element[0].match(/scopeoperation/g)) 
                {
                    if(element[1] instanceof Array)
                    {
                        element[1].forEach(obj => {
                            let PreScopeOp = obj.toString().split("|#$%");
                            let ScopeType = PreScopeOp[0].toString().split("/");
                            let ScopeOpObj = {
                                Scope: ScopeType[1],
                                Type: ScopeType[0],
                                Operation: PreScopeOp[1]
                            }
                            ScopeOperations.push(ScopeOpObj);
                        });
                    }
                    else
                    {
                        let PreScopeOp = element[1].toString().split("|#$%");
                        let ScopeType = PreScopeOp[0].toString().split("/");
                        let ScopeOpObj = {
                            Scope: ScopeType[1],
                            Type: ScopeType[0],
                            Operation: PreScopeOp[1]
                        }
                        ScopeOperations.push(ScopeOpObj);
                    }
                    
                }
            }

            return ScopeOperations;
        } 
        else 
        {
            return null;
            // this.errorSrv.add(420, 'Tentantiva de getClaims um token inválido')
        }
    }

    private getScopeRoles(tk?: Token.Token): string[] {

        let ScopeRoles =  [];

        if ((tk === null) || (tk === undefined)) {
            tk = this.tokenSrv.getToken()
        }

        if (this.tokenSrv.isValid(tk)) 
        {
            const ArJwt: any = Object.entries(this.tokenSrv.getDecoded());
        
            for (let i = 0; i < ArJwt.length; i++) {
                const element = ArJwt[i];
                if (element[0].match(/scoperole/g)) {

                    if(element[1] instanceof Array)
                    {
                        element[1].forEach(obj => {
                            let PreScopeRole = obj.toString().split("|#$%");
                            let ScopeType = PreScopeRole[0].toString().split("/");
                            let ScopeRoleObj = {
                                Scope: ScopeType[1],
                                Type: ScopeType[0],
                                Role: PreScopeRole[1]
                            }
                            ScopeRoles.push(ScopeRoleObj);
                        });
                    }
                    else
                    {
                        let PreScopeRole = element[1].toString().split("|#$%");
                        let ScopeType = PreScopeRole[0].toString().split("/");
                        let ScopeRoleObj = {
                            Scope: ScopeType[1],
                            Type: ScopeType[0],
                            Role: PreScopeRole[1]
                        }
                        ScopeRoles.push(ScopeRoleObj);
                    }

                }
            }

            return ScopeRoles;
        } 
        else 
        {
            return null;
            // this.errorSrv.add(420, 'Tentantiva de getClaims um token inválido')
        }
    }

    private getScopes(tk?: Token.Token): string[] 
    {

        let Scopes =  [];

        if ((tk === null) || (tk === undefined)) {
            tk = this.tokenSrv.getToken()
        }

        if (this.tokenSrv.isValid(tk)) 
        {
            const ArJwt: any = Object.entries(this.tokenSrv.getDecoded());
        
            for (let i = 0; i < ArJwt.length; i++) {
                const element = ArJwt[i];
                if (element[0].match(/scope$/g)) {

                    if(element[1] instanceof Array)
                    {
                        element[1].forEach(obj => {
                            let SpTp = obj.split("/");
                            let ScopeObj = {
                                Scope: SpTp[1],
                                Type: SpTp[0]
                            }
                            Scopes.push(ScopeObj);
                        });
                    }
                    else
                    {
                        let SpTp = element[1].split("/");
                        let ScopeObj = {
                            Scope: SpTp[1],
                            Type: SpTp[0]
                        }
                        Scopes.push(ScopeObj);
                    }

                }
            }

            return Scopes;
        } 
        else 
        {
            return null;
            // this.errorSrv.add(420, 'Tentantiva de getClaims um token inválido')
        }
    }

    private returnDefaultScope(scope: string){
        if(scope == "" || scope == null || scope == undefined || scope == "undefined")
        {
            return Config.ScopeTypeDefault.Scope;
        }
        return scope;
    }

    private returnDefaultType(type: string){
        if(type == "" || type == null || type == undefined || type == "undefined")
        {
            return Config.ScopeTypeDefault.Type;
        }
        return type;
    }  
}
