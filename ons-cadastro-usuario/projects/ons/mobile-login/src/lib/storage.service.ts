import { Storage } from '@ionic/storage';
import { Injectable } from '@angular/core';

import { Subject } from 'rxjs/Subject';
import 'rxjs/add/observable/of';


/** SERVICES */
import { ErrorService } from './error.service';
import { Config } from './../environment/config';

/**
 * Classe responsavel pela gravação dos dados no device
 * 
 */

@Injectable()
/**
 * Classe responsavel por todas as operações no storage.
 */
export class StorageService {

    public onStorageError = new Subject<any>();
    public onStorageOk = new Subject<any>();


    constructor(public storage: Storage
        , public errorSrv: ErrorService) {
    }

    /**
     * Inclui um dado no storage
     * @param key Chave do dado
     * @param value Valor a ser armazenado
     */
    Gravar(key: string, value: any): Promise<any> {
        return this.storage.set(key, value).then((resultado) => {
            this.onStorageOk.next(resultado);
        }).catch((err) => {
            this.errorSrv.add(Config.Error.STORAGE_RECORD_ERROR, 'erro de gravação do token');
            this.onStorageError.next(err);
        });
    }

    /**
     * Recupera um dado armazenado no storage.
     * Em caso de erro um valor null será retornado e o evento de erro será ativado
     * @param key Chave do dado a ser recuperado
     */
    recuperar(key: any): Promise<any> {
        return this.storage.get(key).then((resultado) => {
            this.onStorageOk.next(resultado);
            return resultado;
        }).catch((err) => {
            this.onStorageError.next(err);
            return null;
        });
    }

    /**
     * TALVEZ TENHAMOS DE EXCLUIR ESTE METODO
     * @param key
     */
    recuperarToken(key: any): Promise<any> {
        return this.storage.get(key).then((resultado) => {
            this.onStorageOk.next(resultado);
            return resultado;
        }).catch((err) => {
            this.onStorageError.next(err);
            return null;
        });
    }

    /**
     * Exclui definitivamente um dado do storage.
     * @param key Chave do dado a ser excluído
     */
    deletar(key: string) {
        this.storage.remove(key).then((resultado) => {
            this.onStorageOk.next(resultado);
        }).catch((err) => {
            this.onStorageError.next(err);
        });
    }

    /**
     * Limpa todos os dados armazenados no Device
     */
    limparBase() {
        this.storage.clear().then((resultado) => {
            console.log('Storage Clear ', resultado);

        }).catch((err) => {
            console.log('error on limpar base', err);

            this.errorSrv.add(Config.Error.STORAGE_CLEAN_ERROR, Config.mensagemGenerica.STORAGE_CLEAN_ERROR);
        });
    }
}
