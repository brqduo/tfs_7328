import { Injectable } from '@angular/core';
/**
 * A finalidade desta classe é tratar as imagens utilizadas nas paginas de login.
 * Estas imagens são utilizadas nas mudanças de background definidas no app.
 * Esta funcionalidade faz-se necessária pois existe uma diferença na utilização deste background
 * quando o mesmo é oriundo de um package.
 * 
 */
@Injectable()
export class ImageService {

    private _imageArray = [];

    constructor() {
        this._imageArray = [];
        this.SetDefault();
    }

    /**
     * Retorna as imagens configuradas para exibição
     */
    get Images() {
        return this._imageArray;
    }

    /**
     * Inclui uma imagem na lista de exibição
     * @param img Imagem a ser incluida ('path')
     */
    public SetImage(img: string) {
        this._imageArray.push(img);
    }

    /**
     * Limpa a lista das imagens atuais
     */
    public Clear() {
        this._imageArray = [];
    }

    /**
     * Configura as imagens default da aplicação.
     * São definidas 4 imagems padrões. 
     *
     */
    public SetDefault() {
        this.Clear();
        this._imageArray.push('./assets/login/background-1.jpg');
        this._imageArray.push('./assets/login/background-2.jpg');
        this._imageArray.push('./assets/login/background-3.jpg');
        this._imageArray.push('./assets/login/background-4.jpg');
    }

}
