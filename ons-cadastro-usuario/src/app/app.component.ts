import { Component } from '@angular/core';
import { AnalyticsService } from '@ons/ons-mobile-analytics';
// import { } from '@ons/MobileLogin';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'PackageApp';

  constructor(public srv: AnalyticsService){
    this.srv.testeConsole();
  }
}
