import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { OnsCadastroUsuarioModule } from '@ons/ons-cadastro-usuario';
import { OnsAnalyticsModule } from '@ons/ons-mobile-analytics';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    OnsAnalyticsModule.forRoot(),
    OnsCadastroUsuarioModule
  ],
  providers: [
    
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
