webpackJsonp([0],{

/***/ 136:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CadastroPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(67);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__home_home__ = __webpack_require__(253);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services_ons_cadastro_usuario_service__ = __webpack_require__(254);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_inputmask__ = __webpack_require__(680);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_inputmask___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_inputmask__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var CadastroPage = /** @class */ (function () {
    function CadastroPage(navCtrl, onsSrv) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.onsSrv = onsSrv;
        this.passwordType = 'password';
        this.passwordIcon = 'eye-off';
        this.Novousuario = {
            nome: '',
            cpf: '',
            email: '',
            senha: '',
            telefone: {
                ddi: '',
                ddd: '',
                numero: ''
            },
            tipoPerfil: '',
            tipoAgente: ''
        };
        this.checkbox = false;
        this.perfisParaExibir = [];
        this.perfilTipo = [];
        this.codigoReCaptcha = '6LdqEqEUAAAAAMU5YkbfddoMlg9R4iQ8sU7gqriE';
        this.userChanged = false;
        this.errorLabels = {
            nome: '',
            cpf: '',
            email: '',
            telefone: '',
        };
        this.selectedOption = {
            title: 'Tipo de Perfil',
        };
        this.showNHide = {
            SelectBox: false,
            EmpresaLink: false
        };
        this.versaoApp = "";
        this.recaptchaValidate = false;
        this.links = {
            PoliticaPrivacidade: 'http://dsvportalrelacionamento.ons.org.br/sites/cadastro/paginas/condicoes-de-uso.aspx',
            Empresa: 'http://dsvportalrelacionamento.ons.org.br/sites/cadastro/paginas/cadastro-de-empresa.aspx'
        };
        var img = 'https://poptst.ons.org.br/ons.pop.federation/Content/img/background.png';
        this.Fundo_Imagem = {
            'background': '#156FAB url(' + img + ')'
        };
        this.onsSrv.carregarPerfis().subscribe(function (res) {
            _this.perfisParaExibir = res.publico;
            _this.perfilTipo = [
                {
                    nome: 'Estudante, público, etc..Conteúdo descrevendo os perfis que devem selecionar esta opção.',
                    valor: 'Estudante'
                },
                {
                    nome: 'Nova empresa. Conteúdo descrevendo as situações em que esta opção deve ser selecionada.',
                    valor: 'Empresa'
                }
            ];
        });
    }
    CadastroPage.prototype.hideShowPassword = function () {
        console.log('sendo ativado');
        this.passwordType = this.passwordType === 'text' ? 'password' : 'text';
        this.passwordIcon = this.passwordIcon === 'eye-off' ? 'eye' : 'eye-off';
    };
    CadastroPage.prototype.logar = function () {
        console.log("Entrando", this.form);
        console.log("Entrando", this.Novousuario);
        console.log(this.recaptchaValidate);
        if (this.form.valid && this.recaptchaValidate) {
            this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_3__home_home__["a" /* HomePage */]);
        }
        else {
            console.log('tente novamente');
        }
    };
    CadastroPage.prototype.setPerfil = function (item, event) {
        console.log(event);
        this.Novousuario.tipoPerfil = item.valor;
        if (item.valor === 'Empresa') {
            this.showNHide.EmpresaLink = true;
        }
        else {
            this.showNHide.EmpresaLink = false;
        }
    };
    CadastroPage.prototype.onBlur = function (ev, emailChildObj) {
        var _this = this;
        console.log(ev);
        if (emailChildObj.errors === null) {
            this.onsSrv.verificaEmailOns(this.Novousuario.email).subscribe(function (res) {
                console.log(res.dominioEConhecido);
                if (res.dominioEConhecido) {
                    _this.showNHide.SelectBox = true;
                }
                else {
                    _this.showNHide.SelectBox = false;
                }
            });
        }
        else {
            this.showNHide.SelectBox = false;
        }
    };
    CadastroPage.prototype.validarInputs = function (event) {
        console.log(event);
        if (event.touched && event.errors !== null) {
            switch (event.name) {
                case 'nome':
                    console.log('nome ativado');
                    this.errorLabels.nome = 'Este campo é obrigatório';
                    break;
                case 'cpf':
                    console.log('cpf ativado');
                    if (event.errors.minlength !== null) {
                        this.errorLabels.cpf = 'o documento informado é invalido';
                    }
                    if (event.errors.maxlength !== null) {
                        this.errorLabels.cpf = 'o documento informado é invalido';
                    }
                    break;
                case 'email':
                    console.log('email ativado');
                    if (event.errors.email) {
                        this.errorLabels.email = 'Este email não é valido';
                    }
                    break;
                case 'telefoneNovoNumero':
                    this.errorLabels.telefone = 'Este campo é obrigatório';
                    break;
                case 'senha':
                    break;
                case 'tipoDePerfil':
                    break;
            }
        }
    };
    CadastroPage.prototype.setMask = function () {
        var mask = {
            cpf: "999.999.999-99",
            passaporte: "**-9{4,10}",
            telefone: "999999999"
        };
        var telefoneMask = new __WEBPACK_IMPORTED_MODULE_5_inputmask___default.a(mask.passaporte);
        var passaporteMask = new __WEBPACK_IMPORTED_MODULE_5_inputmask___default.a(mask.passaporte);
        var cpfMask = new __WEBPACK_IMPORTED_MODULE_5_inputmask___default.a(mask.cpf);
        var cpf = document.getElementById('cpf');
        console.log(this.cpfElement);
        var cpfLength = this.cpfElement.value.length;
        console.log(cpfLength);
        console.log(cpfLength);
        if (cpfLength <= 0) {
            __WEBPACK_IMPORTED_MODULE_5_inputmask___default.a.remove(cpf);
        }
        if (cpfLength < 11) {
            passaporteMask.mask(cpf);
        }
        if (cpfLength >= 11) {
            cpfMask.mask(cpf);
        }
    };
    CadastroPage.prototype.resolved = function (ev) {
        console.log(ev);
        if (ev !== null || ev !== undefined || ev !== '') {
            this.recaptchaValidate = true;
        }
    };
    CadastroPage.prototype.testeTelefone = function (v) {
        console.log('replace this', v);
        var mask = "99999-9999";
        var telefoneMask = new __WEBPACK_IMPORTED_MODULE_5_inputmask___default.a({ mask: mask, greedy: false });
        var telefone = document.getElementById('telefone');
        telefoneMask.mask(telefone);
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('RegisterForm'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_2__angular_forms__["d" /* NgForm */])
    ], CadastroPage.prototype, "form", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('NovousuarioCPF'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_2__angular_forms__["e" /* NgModel */])
    ], CadastroPage.prototype, "cpfElement", void 0);
    CadastroPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-cadastro',template:/*ion-inline-start:"/brq/TFS_7328_Bitbucket/FrontEnd/src/pages/cadastro/cadastro.html"*/'<ion-content class="transparent-header">\n    <div class="fundo-imagem" [ngStyle]="Fundo_Imagem"></div>\n    <div class="MainFrame">\n        <div class="imgTop">\n            <img src="./assets/login/logo-ons.png">\n        </div>\n        <div class="inputBox">\n            <h2>Cadastro de Usuário</h2>\n            <span id="header" class="aviso">Todos os campos são de preenchimento obrigatório</span>\n            <form id=LoginForm (ngSubmit)="logar()" #RegisterForm="ngForm">\n                <button ion-button block outline type="submit" color="light" class="login-button"\n                    *ngIf="!showNHide.EmpresaLink">Solicitar\n                    Cadastro\n                </button>\n                <div class="onsInput">\n                    <label class="normal" for="nome">Nome</label>\n                    <input type="text" name="nome" id="nome" required [(ngModel)]="Novousuario.nome"\n                        #NovousuarioNome="ngModel" class="valid">\n                    <label class="error" [hidden]="isNameValid(NovousuarioNome)">\n                        {{errorLabels.nome}}dsd\n                    </label>\n                </div>\n                <div class="onsInput">\n                    <label class="normal" for="cpf">CPF ou Passaporte</label>\n                    <input type="text" name="cpf" id="cpf" required [(ngModel)]="Novousuario.cpf"\n                        (ngModelChange)="setarMascara(Novousuario.cpf)" maxlength="14">\n                    <label class="error cpf" [hidden]="IsValido()">\n                        {{errorLabels.cpf}}\n                    </label>\n                    <span class="aviso">Caso você não seja brasileiro, preencha com seu passaporte</span>\n                </div>\n                <div class="onsInput">\n                    <label class="normal" for="email">Email</label>\n                    <input type="text" name="email" id="email" class="valid" [(ngModel)]="Novousuario.email"\n                        #NovousuarioEmail="ngModel" required email (blur)="onBlur($event, NovousuarioEmail)">\n                    <label class="error" [hidden]="!isEmailValid(NovousuarioEmail)">\n                        {{errorLabels.email}}\n                    </label>\n                </div>\n                <div class="onsInput">\n                    <label class="normal" for="telefone">Telefone Comercial</label>\n                    <input type="number" name="telefoneNovoDDI" [(ngModel)]="Novousuario.telefone.ddi" id="ddi"\n                        placeholder="ddi" #NovousuarioTelefoneNovoDDi="ngModel" required max="999" required>\n                    <input type="number" name="telefoneNovoDDD" [(ngModel)]="Novousuario.telefone.ddd" max="99" id="ddd"\n                        placeholder="ddd" #NovousuarioTelefoneNovoDDD="ngModel" required>\n\n                    <input type="text" name="telefoneNovoNumero" [(ngModel)]="Novousuario.telefone.numero" id="telefone"\n                        placeholder="telefone" (ngModelChange)="validarInputs(NovousuarioTelefoneNovoNumero)"\n                        (blur)="validarTelefone()" #NovousuarioTelefoneNovoNumero="ngModel" required>\n\n\n                    <label class="error"\n                        *ngIf="NovousuarioTelefoneNovoNumero.invalid && NovousuarioTelefoneNovoNumero.touched">\n                        {{errorLabels.telefone}}\n                    </label>\n                </div>\n                <div class="onsInput">\n                    <label class="normal" for="senha">Senha</label>\n                    <div class="senhaBtn">\n                        <input [type]="passwordType" name="senha" id="senha" [(ngModel)]="Novousuario.senha" required\n                            #NovousuarioSenha="ngModel">\n                        <button ion-button clear (click)="hideShowPassword()" type="button">\n                            <ion-icon [name]="passwordIcon" class="iconButton"></ion-icon>\n                        </button>\n                    </div>\n                </div>\n                <div *ngIf="showNHide.SelectBox">\n                    <div class="onsInput">\n                        <label class="normal" for="tipoPerfil">Selecione o seu perfil</label>\n                        <div>\n                            <label class="onsRadioContainer" *ngFor="let item of perfilTipo">{{item.nome}}\n                                <input type="radio" name="radio" #radioPerfil (click)="setPerfil(item, radioPerfil)">\n                                <span class="checkmark"></span>\n                            </label>\n                        </div>\n                        <ion-item>\n                            <ion-select [selectOptions]="selectedOption" id="tipoPerfil" multiple="false"\n                                name="tipoDePerfil1" [(ngModel)]="Novousuario.tipoAgente"\n                                #NovousuarioTipoPerfil1="ngModel" placeholder="Tipos de perfil"\n                                *ngIf="!showNHide.EmpresaLink">\n                                <ion-option [value]="item" *ngFor="let item of perfisParaExibir">{{item.nome}}\n                                </ion-option>\n                            </ion-select>\n                        </ion-item>\n                    </div>\n                </div>\n\n                <div class="captcha" *ngIf="!showNHide.EmpresaLink">\n                    <re-captcha (resolved)="resolved($event)" siteKey="6LebGqEUAAAAAJGvLfMvMd5AhRWvt6Sq7S01lKlH">\n                    </re-captcha>\n                </div>\n                <hr>\n                <div class="checkbox-Box" *ngIf="!showNHide.EmpresaLink">\n                    <label class="accept_63f916e8">\n                        <input type="checkbox" id="accept" name="accept" [(ngModel)]="checkbox" required>\n                        Ao criar uma conta, você concorda com as\n                        <a class="" target="_blank" [href]="links.PoliticaPrivacidade">Condições\n                            de uso e Política de privacidade do ONS</a></label>\n                </div>\n                <button ion-button block outline type="submit" color="light" class="login-button"\n                    *ngIf="!showNHide.EmpresaLink">Solicitar\n                    Cadastro\n                </button>\n                <div *ngIf="showNHide.EmpresaLink" class="rotaEmpresa">\n                    <a class="link_63f916e8" target="_blank" [href]="links.Empresa">O cadastro de empresas deve ser\n                        efetuado por este link </a>\n                </div>\n            </form>\n        </div>\n        <!-- (click)="logar()"  -->\n        <div class="footerBar">\n            <div>\n                <img src="./assets/login/logo-ons.png">\n                <br>\n                <span>© 2008-2019 - {{ versaoApp }} 1.1.1</span>\n            </div>\n        </div>\n    </div>\n</ion-content>'/*ion-inline-end:"/brq/TFS_7328_Bitbucket/FrontEnd/src/pages/cadastro/cadastro.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* NavController */], __WEBPACK_IMPORTED_MODULE_4__services_ons_cadastro_usuario_service__["a" /* OnsCadastroUsuarioService */]])
    ], CadastroPage);
    return CadastroPage;
}());

//# sourceMappingURL=cadastro.js.map

/***/ }),

/***/ 163:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 163;

/***/ }),

/***/ 207:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 207;

/***/ }),

/***/ 253:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HomePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(67);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__cadastro_cadastro__ = __webpack_require__(136);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var HomePage = /** @class */ (function () {
    function HomePage(navCtrl) {
        this.navCtrl = navCtrl;
        this.passwordType = 'password';
        this.backgrounds = [
            './assets/login/background-1.jpg',
            './assets/login/background-2.jpg',
            './assets/login/background-3.jpg',
            './assets/login/background-4.jpg',
        ];
        this.passwordIcon = 'eye-off';
        this.usuario = {
            UserName: 'ONS\\', UserPass: '', Changed: false, passwordType: 'password', passwordIcon: 'eye-off'
        };
        this.userChanged = false;
        this.versaoApp = "";
        // const img = 'http://twixar.me/vtgK';
        var img = 'https://poptst.ons.org.br/ons.pop.federation/Content/img/background.png';
        this.Fundo_Imagem = {
            'background': '#156FAB url(' + img + ')'
        };
    }
    HomePage.prototype.usuarioAlterado = function (x) {
        this.userChanged = true;
        // this.loginSrv.Usuario_alterado = this.userChanged;
    };
    HomePage.prototype.hideShowPassword = function () {
        this.passwordType = this.passwordType === 'text' ? 'password' : 'text';
        this.passwordIcon = this.passwordIcon === 'eye-off' ? 'eye' : 'eye-off';
    };
    HomePage.prototype.logar = function (ev) {
        console.log("Entrando", ev);
    };
    HomePage.prototype.goToCadastro = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__cadastro_cadastro__["a" /* CadastroPage */]);
    };
    HomePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-home',template:/*ion-inline-start:"/brq/TFS_7328_Bitbucket/FrontEnd/src/pages/home/home.html"*/'<ion-content class="transparent-header">\n  <!-- <img src="http://twixar.me/vtgK" class="fundo-imagem" /> -->\n  <div class="fundo-imagem" [ngStyle]="Fundo_Imagem"></div>\n  <div class="MainFrame">\n    <div class="imgTop">\n      <img src="./assets/login/logo-ons.png">\n    </div>\n    <div class="inputBox">\n      <h2>Entrar</h2>\n      <form (ngSubmit)="logar(RegisterForm)" #RegisterForm="ngForm">\n        <div class="onsInput">\n          <label class="normal" for="nome">Usuário ou E-mail</label>\n          <input type="text" name="nome" id="nome" required [(ngModel)]="usuario.UserName" #NovousuarioNome="ngModel"\n            class="valid">\n        </div>\n        <div class="onsInput">\n          <label class="normal" for="senha">Senha</label>\n          <input type="password" name="senha" id="senha" [(ngModel)]="usuario.UserPass" required\n            #NovousuarioSenha="ngModel">\n        </div>\n        <a class="esqueciSenha" href="https://poptst.ons.org.br/ons.pop.federation/passwordrecovery/?ReturnUrl=https%3a%2f%2fpoptst.ons.org.br%2fons.pop.federation%2f%3fReturnUrl%3dhttp%253a%252f%252fpoptst.ons.org.br%252fpop%252f"> Esqueci a Senha</a>\n        <hr>\n        <button ion-button block outline type="submit" color="light" class="login-button">Entrar</button>\n        <button ion-button block outline (click)="goToCadastro()" color="light" class="login-button new">Novo cadastro\n        </button>\n      </form>\n    </div>\n    <div class="footerBar">\n      <div>\n        <img src="./assets/login/logo-ons.png">\n        <br>\n        <span>© 2008-2019 - {{ versaoApp }} 1.1.1</span>\n      </div>\n    </div>\n  </div>\n</ion-content>'/*ion-inline-end:"/brq/TFS_7328_Bitbucket/FrontEnd/src/pages/home/home.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* NavController */]])
    ], HomePage);
    return HomePage;
}());

//# sourceMappingURL=home.js.map

/***/ }),

/***/ 254:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return OnsCadastroUsuarioService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common_http__ = __webpack_require__(255);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs__ = __webpack_require__(258);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__config__ = __webpack_require__(679);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var OnsCadastroUsuarioService = /** @class */ (function () {
    function OnsCadastroUsuarioService(http) {
        this.http = http;
        this._permiteNovoUsuario = false;
        this.tst();
    }
    Object.defineProperty(OnsCadastroUsuarioService.prototype, "permiteNovoUsuario", {
        get: function () {
            return this._permiteNovoUsuario;
        },
        set: function (value) {
            this._permiteNovoUsuario = value;
        },
        enumerable: true,
        configurable: true
    });
    OnsCadastroUsuarioService.prototype.salvarUsuario = function () {
        return __WEBPACK_IMPORTED_MODULE_2_rxjs__["Observable"].of({
            id: 0,
            login: "nilfton.jacintho@gmail.com",
            sid: "S-1-9-1089059503-1749794877-1963312245",
            nome: "teste-brq"
        }); //this.http.post("",{});
    };
    OnsCadastroUsuarioService.prototype.salvarUsuarioErro = function () {
        return __WEBPACK_IMPORTED_MODULE_2_rxjs__["Observable"].of({
            "codigo": 400,
            "menssagem": "O campo Documento não possui numero de caracteres válidos 000.000.000-00 (cpf).\r\nO campo Documento não possui numero de caracteres válidos 000.000.000-00 (cpf)."
        });
        //this.http.post("",{});
    };
    OnsCadastroUsuarioService.prototype.carregarCondicoes = function () {
        return this.http.get("");
    };
    OnsCadastroUsuarioService.prototype.carregarPerfis = function () {
        return this.http.get(__WEBPACK_IMPORTED_MODULE_3__config__["a" /* config */].TST.URL_SINTEGRE + __WEBPACK_IMPORTED_MODULE_3__config__["a" /* config */].TST.URL_SINTEGRE_PROXYS.listaPerfilPublico);
    };
    OnsCadastroUsuarioService.prototype.verificaEmailOns = function (email) {
        return this.http.get(__WEBPACK_IMPORTED_MODULE_3__config__["a" /* config */].TST.URL_SINTEGRE + __WEBPACK_IMPORTED_MODULE_3__config__["a" /* config */].TST.URL_SINTEGRE_PROXYS.verificaEmail + email);
    };
    OnsCadastroUsuarioService.prototype.tst = function () {
        console.log('entrando em tst');
        this.http.get(__WEBPACK_IMPORTED_MODULE_3__config__["a" /* config */].TST.URL_SINTEGRE + __WEBPACK_IMPORTED_MODULE_3__config__["a" /* config */].TST.URL_SINTEGRE_PROXYS.listaPerfilPublico).subscribe(function (res) {
            console.log(res);
        });
    };
    OnsCadastroUsuarioService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_common_http__["a" /* HttpClient */]])
    ], OnsCadastroUsuarioService);
    return OnsCadastroUsuarioService;
}());

//# sourceMappingURL=ons-cadastro-usuario.service.js.map

/***/ }),

/***/ 351:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__(352);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_module__ = __webpack_require__(356);


Object(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_1__app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 356:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__(42);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(67);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(247);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_status_bar__ = __webpack_require__(252);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__app_component__ = __webpack_require__(398);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__pages_home_home__ = __webpack_require__(253);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__pages_cadastro_cadastro__ = __webpack_require__(136);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__angular_common_http__ = __webpack_require__(255);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9_ng_recaptcha__ = __webpack_require__(685);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9_ng_recaptcha___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_9_ng_recaptcha__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__services_ons_cadastro_usuario_service__ = __webpack_require__(254);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};











var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_5__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_6__pages_home_home__["a" /* HomePage */],
                __WEBPACK_IMPORTED_MODULE_7__pages_cadastro_cadastro__["a" /* CadastroPage */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_8__angular_common_http__["b" /* HttpClientModule */],
                __WEBPACK_IMPORTED_MODULE_9_ng_recaptcha__["RecaptchaModule"].forRoot(),
                __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["c" /* IonicModule */].forRoot(__WEBPACK_IMPORTED_MODULE_5__app_component__["a" /* MyApp */], {}, {
                    links: []
                })
            ],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["a" /* IonicApp */]],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_5__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_6__pages_home_home__["a" /* HomePage */],
                __WEBPACK_IMPORTED_MODULE_7__pages_cadastro_cadastro__["a" /* CadastroPage */]
            ],
            providers: [
                __WEBPACK_IMPORTED_MODULE_4__ionic_native_status_bar__["a" /* StatusBar */],
                __WEBPACK_IMPORTED_MODULE_10__services_ons_cadastro_usuario_service__["a" /* OnsCadastroUsuarioService */],
                __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */],
                { provide: __WEBPACK_IMPORTED_MODULE_1__angular_core__["ErrorHandler"], useClass: __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["b" /* IonicErrorHandler */] }
            ]
        })
    ], AppModule);
    return AppModule;
}());

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 398:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyApp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(67);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__ = __webpack_require__(252);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(247);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_cadastro_cadastro__ = __webpack_require__(136);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var MyApp = /** @class */ (function () {
    function MyApp(platform, statusBar, splashScreen) {
        this.rootPage = __WEBPACK_IMPORTED_MODULE_4__pages_cadastro_cadastro__["a" /* CadastroPage */];
        platform.ready().then(function () {
            // Okay, so the platform is ready and our plugins are available.
            // Here you can do any higher level native things you might need.
            statusBar.styleDefault();
            splashScreen.hide();
        });
    }
    MyApp = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({template:/*ion-inline-start:"/brq/TFS_7328_Bitbucket/FrontEnd/src/app/app.html"*/'<ion-nav [root]="rootPage"></ion-nav>\n'/*ion-inline-end:"/brq/TFS_7328_Bitbucket/FrontEnd/src/app/app.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* Platform */], __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__["a" /* StatusBar */], __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */]])
    ], MyApp);
    return MyApp;
}());

//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ 679:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return config; });
var config = {
    TST: {
        URL_SINTEGRE: "http://tst-git-065.ons.org.br:83/ONS.Sintegre.Proxy",
        URL_SINTEGRE_PROXYS: {
            verificaEmail: "/proxy/cadastro/verificaemail?email=",
            dominioConhecido: "/proxy/cadastro/publicossemdominio",
            aceiteCadastro: "/api/aceite",
            cadastroExterno: "/proxy/cadastro/cadastroexterno",
            listaPerfilPublico: "/proxy/cadastro/publicos?filtro=SemDominio"
        },
        recaptchaKey: '6LdqEqEUAAAAAMU5YkbfddoMlg9R4iQ8sU7gqriE',
        linksExternos: {
            PoliticaPrivacidade: 'http://dsvportalrelacionamento.ons.org.br/sites/cadastro/paginas/condicoes-de-uso.aspx',
            Empresa: 'http://dsvportalrelacionamento.ons.org.br/sites/cadastro/paginas/cadastro-de-empresa.aspx'
        },
        backgroundIMG: 'https://poptst.ons.org.br/ons.pop.federation/Content/img/background.png'
    }
};
//# sourceMappingURL=config.js.map

/***/ })

},[351]);
//# sourceMappingURL=main.js.map