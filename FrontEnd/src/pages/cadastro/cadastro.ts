import { Component, ViewChild, ElementRef } from '@angular/core';
import { NavController } from 'ionic-angular';
import { newUserInterface } from '../../interfaces/newUser.model';
import { NgForm, NgModel, AbstractControl, FormGroup } from '@angular/forms';
import { HomePage } from '../home/home';
import { OnsCadastroUsuarioService } from '../../services/ons-cadastro-usuario.service';
import Inputmask from "inputmask";
import { EventManager } from '@angular/platform-browser';
import { elementAt } from 'rxjs/operators';
import { config } from '../../services/config';

@Component({
  selector: 'page-cadastro',
  templateUrl: 'cadastro.html'
})
export class CadastroPage {
  passwordType = 'password';
  Fundo_Imagem: any;
  passwordIcon = 'eye-off';
  public Novousuario: newUserInterface = {
    nome: '',
    cpf: '',
    email: '',
    senha: '',
    captcha: '',
    telefone: {
      ddi: '',
      ddd: '',
      numero: ''
    },
    tipoPerfil: '',
    tipoAgente: ''
  };
  checkbox = false;
  perfisParaExibir = [];
  perfilTipo = [];
  codigoReCaptcha = config.TST.recaptchaKey
  userChanged = false;
  errorLabels = {
    nome: '',
    cpf: '',
    email: '',
    telefone: '',
    senha: '',
  }
  selectedOption = {
    title: 'Tipo de Perfil',
  }
  showNHide = {
    SelectBox: false,
    EmpresaLink: false
  }
  versaoApp: string = "";
  @ViewChild('RegisterForm') form: NgForm;
  @ViewChild('NovousuarioCPF') cpfElement: NgModel;
  recaptchaValidate = false;
  links = {
    PoliticaPrivacidade: config.TST.linksExternos.PoliticaPrivacidade,
    Empresa: config.TST.linksExternos.Empresa
  }

  constructor(public navCtrl: NavController, public onsSrv: OnsCadastroUsuarioService) {


    let img = 'https://poptst.ons.org.br/ons.pop.federation/Content/img/background.png';
    this.Fundo_Imagem = {
      'background': '#156FAB url(' + img + ')'
    }
    this.onsSrv.carregarPerfis().subscribe((res) => {
      this.perfisParaExibir = res.publico;
      this.perfilTipo = [
        {
          nome: 'Estudante, público, etc..Conteúdo descrevendo os perfis que devem selecionar esta opção.',
          valor: 'Estudante'
        },
        {
          nome: 'Nova empresa. Conteúdo descrevendo as situações em que esta opção deve ser selecionada.',
          valor: 'Empresa'
        }
      ]
    });

  }

  hideShowPassword() {
    console.log('sendo ativado');
    this.passwordType = this.passwordType === 'text' ? 'password' : 'text';
    this.passwordIcon = this.passwordIcon === 'eye-off' ? 'eye' : 'eye-off';
  }

  logar() {
    console.log("Entrando", this.form);
    console.log("Entrando os obj de gravação", this.Novousuario);
    console.log(this.recaptchaValidate)

    const resToSend = {
      nome: this.Novousuario.nome,
      documento: this.Novousuario.cpf,
      email: this.Novousuario.email,
      senha: this.Novousuario.senha,
      tipoPerfil: {
        id: this.Novousuario.tipoAgente,
        nome: this.Novousuario.tipoPerfil,
      },
      captcha: this.Novousuario.captcha,
      telefone: {
        ddi: this.Novousuario.telefone.ddi,
        ddd: this.Novousuario.telefone.ddd,
        numero: this.Novousuario.telefone.numero,
      }
    }
    console.log(resToSend);

    if (this.form.valid && this.recaptchaValidate) {
      this.navCtrl.setRoot(HomePage)
    } else {
      console.log('tente novamente')
    }

  }

  IsValido() {
    if (this.Novousuario.cpf == null || this.Novousuario.cpf == undefined || this.Novousuario.cpf.toString().trim() == "") {
      return false;
    }

    let cpfOrPass = this.Novousuario.cpf.split('.').join("").split('-').join("");


    if (cpfOrPass.length > 10) {

      if (!this.IsCpfValido(cpfOrPass) && cpfOrPass.trim() != "") {
        this.errorLabels.cpf = 'O documento informado é invalido'
      }
      else {
        this.errorLabels.cpf = "";
      }
      return this.IsCpfValido(cpfOrPass);
    }
    else {
      if (!this.IsPassaporteValido(cpfOrPass) && cpfOrPass.trim() != "") {
        this.errorLabels.cpf = 'O documento informado é invalido'
      }
      else {
        this.errorLabels.cpf = "";
      }
      return this.IsPassaporteValido(cpfOrPass);
    }
  }

  IsPassaporteValido(pass: string) {
    let exp = new RegExp("^(?!^0+$)[a-zA-Z0-9]{3,10}$");

    return exp.test(pass);
  }

  IsCpfValido(cpf: string): boolean {
    if (cpf == null || cpf == undefined || cpf.toString().trim() == "") {
      return false;
    }

    cpf = cpf.split('.').join("").split('-').join("");

    if (cpf.length != 11) {
      return false;
    }
    if ((cpf == '00000000000') || (cpf == '11111111111') || (cpf == '22222222222') || (cpf == '33333333333') || (cpf == '44444444444') || (cpf == '55555555555') || (cpf == '66666666666') || (cpf == '77777777777') || (cpf == '88888888888') || (cpf == '99999999999')) {
      return false;
    }
    let numero: number = 0;
    let caracter: string = '';
    let numeros: string = '0123456789';
    let j: number = 10;
    let somatorio: number = 0;
    let resto: number = 0;
    let digito1: number = 0;
    let digito2: number = 0;
    let cpfAux: string = '';
    cpfAux = cpf.substring(0, 9);
    for (let i: number = 0; i < 9; i++) {
      caracter = cpfAux.charAt(i);
      if (numeros.search(caracter) == -1) {
        return false;
      }
      numero = Number(caracter);
      somatorio = somatorio + (numero * j);
      j--;
    }
    resto = somatorio % 11;
    digito1 = 11 - resto;
    if (digito1 > 9) {
      digito1 = 0;
    }
    j = 11;
    somatorio = 0;
    cpfAux = cpfAux + digito1;
    for (let i: number = 0; i < 10; i++) {
      caracter = cpfAux.charAt(i);
      numero = Number(caracter);
      somatorio = somatorio + (numero * j);
      j--;
    }
    resto = somatorio % 11;
    digito2 = 11 - resto;
    if (digito2 > 9) {
      digito2 = 0;
    }
    cpfAux = cpfAux + digito2;
    if (cpf != cpfAux) {
      return false;
    }
    else {
      return true;
    }
  }

  cpfValidator(control: AbstractControl): { [key: string]: boolean } | null {
    if (control.value !== undefined && control.value !== null && control.value.trim() != "") {
      let cpfValido = this.IsCpfValido(control.value);

      return { 'cpfValid': true };
    }
    return null;
  }


  isEmailValid(emailNgModel: NgModel) {
    const emailValidate = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
    const response = emailValidate.test(this.Novousuario.email.toString().toLowerCase())
    console.log(response);
    console.log(emailNgModel.untouched);
    let retorno = false
    if (this.Novousuario.email.toString().toLowerCase().length <= 0) {
      this.errorLabels.email = 'Este campo é Obrigatório';
    } else {
      if (!response) {
        this.errorLabels.email = 'Este email não é valido';
      }
    }

    if (!response && emailNgModel.untouched) {
      retorno = false;
    }
    else {
      retorno = true;
    }
    return retorno;

  }
  isNameValid(nameNgModel: NgModel) {
    let retorno = false;
    let inputVazio = true;
    if (this.Novousuario.nome.toString().toLowerCase().length <= 0) {
      this.errorLabels.nome = 'Este campo é Obrigatório';
      inputVazio = true;
    } else {
      inputVazio = false;
    }
    if (!inputVazio && nameNgModel.untouched) {
      retorno = false;
    }
    else {
      retorno = true;
    }
    return retorno;


  }
  isPassWordValid(passwordNgModel: NgModel) {
    let retorno = false;
    let inputVazio = true;
    if (this.Novousuario.senha.toString().toLowerCase().length <= 0) {
      this.errorLabels.senha = 'Este campo é Obrigatório';
      inputVazio = false;
    } else {
      inputVazio = true;
    }
    if (!inputVazio && passwordNgModel.untouched) {
      retorno = false;
    }
    else {
      retorno = true;
    }
    return retorno;

  }

  setPerfil(item, event) {
    console.log(event);
    this.Novousuario.tipoPerfil = item.valor;
    if (item.valor === 'Empresa') {
      this.showNHide.EmpresaLink = true;
    } else {
      this.showNHide.EmpresaLink = false;
    }
  }

  onBlur(ev, emailChildObj: NgModel) {
    console.log(ev);
    if (emailChildObj.errors === null) {
      this.onsSrv.verificaEmailOns(this.Novousuario.email).subscribe((res) => {
        console.log(res.dominioEConhecido);
        if (res.dominioEConhecido) {
          this.showNHide.SelectBox = true;
        } else {
          this.showNHide.SelectBox = false;
        }
      })
    } else {
      this.showNHide.SelectBox = false;
    }
  }

  setarMascara(valor: string) {
    valor = valor.split('.').join("").split('-').join("");
    if (valor.length > 10) {
      valor = valor.replace(/\D/g, '');
      this.Novousuario.cpf = valor.replace(/(\d{3})(\d{3})(\d{3})(\d{2})/g, "\$1.\$2.\$3\-\$4");
    }
    else {
      this.Novousuario.cpf = valor.replace(/([a-zA-Z0-9]{2})(\d+)/g, "\$1-\$2");
    }
  }

  validarInputs(event: NgModel) {
    console.log(event);

    /*     if (event.name === 'telefoneNovoNumero' && event.value !== '') {
          console.log(event.value);
          this.validarTelefone(event.value)
        } */
    if (event.touched && event.errors !== null) {
      switch (event.name) {
        case 'nome':
          console.log('nome ativado');
          this.errorLabels.nome = 'Este campo é obrigatório'
          break;
        case 'cpf':
          console.log('cpf ativado');
          if (event.errors.minlength !== null) {
            this.errorLabels.cpf = 'o documento informado é invalido'
          }
          if (event.errors.maxlength !== null) {
            this.errorLabels.cpf = 'o documento informado é invalido'
          }
          break;
        case 'email':
          console.log('email ativado');
          if (event.errors.email) {
            this.errorLabels.email = 'Este email não é valido';
          }
          break;
        case 'telefoneNovoNumero':
          this.errorLabels.telefone = 'Este campo é obrigatório'
          break;
        case 'senha':

          break;
        case 'tipoDePerfil':

          break;
      }
    }
  }


  setMask() {
    const mask = {
      cpf: "999.999.999-99",
      passaporte: "**-9{4,10}",
      telefone: "999999999"
    };
    const telefoneMask = new Inputmask(mask.passaporte);
    const passaporteMask = new Inputmask(mask.passaporte);
    const cpfMask = new Inputmask(mask.cpf);
    const cpf = document.getElementById('cpf')
    console.log(this.cpfElement);
    const cpfLength = this.cpfElement.value.length
    console.log(cpfLength);

    console.log(cpfLength);
    if (cpfLength <= 0) {
      Inputmask.remove(cpf);
    }
    if (cpfLength < 11) {
      passaporteMask.mask(cpf)
    }
    if (cpfLength >= 11) {
      cpfMask.mask(cpf)
    }
  }
  resolved(ev) {
    console.log(ev);

    if (ev !== null || ev !== undefined || ev !== '') {
      this.recaptchaValidate = true;
      this.Novousuario.captcha = ev;
    }
  }
  validarTelefone(v) {
    console.log('replace this', v);
    let mask = "99999-9999"
    const telefoneMask = new Inputmask({ mask: mask, greedy: false });
    const telefone = document.getElementById('telefone')
    telefoneMask.mask(telefone);
  }
}

