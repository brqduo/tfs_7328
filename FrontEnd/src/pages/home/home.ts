import { RunTimeUser } from './../../interfaces/runtimeUser.model';
import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { CadastroPage } from '../cadastro/cadastro';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  passwordType = 'password';
  backgrounds = [
    './assets/login/background-1.jpg',
    './assets/login/background-2.jpg',
    './assets/login/background-3.jpg',
    './assets/login/background-4.jpg',
  ];

  Fundo_Imagem: any;
  passwordIcon = 'eye-off';
  public usuario: RunTimeUser = {
    UserName: 'ONS\\', UserPass: '', Changed: false, passwordType: 'password', passwordIcon: 'eye-off'
  };
  userChanged = false;
  versaoApp: string = "";
  constructor(public navCtrl: NavController) {
   // const img = 'http://twixar.me/vtgK';
    let img = 'https://poptst.ons.org.br/ons.pop.federation/Content/img/background.png';
    this.Fundo_Imagem = {
      'background': '#156FAB url(' + img + ')'
    }
  }


  usuarioAlterado(x) {
    this.userChanged = true;
    // this.loginSrv.Usuario_alterado = this.userChanged;
  }

  hideShowPassword() {
    this.passwordType = this.passwordType === 'text' ? 'password' : 'text';
    this.passwordIcon = this.passwordIcon === 'eye-off' ? 'eye' : 'eye-off';
  }

  logar(ev) {
    console.log("Entrando", ev);
  }
  goToCadastro() {
    this.navCtrl.push(CadastroPage)
  }

}
