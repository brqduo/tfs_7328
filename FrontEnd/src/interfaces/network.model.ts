export interface Network
{
    mensagem: string;
    code: number;
    connected: boolean;
}
