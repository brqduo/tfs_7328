export interface newUserInterface {
    nome: string;
    cpf: string;
    email: string;
    senha: string;
    captcha: string;
    telefone: {
        ddi: string;
        ddd: string;
        numero: string;

    },
    tipoPerfil: string;
    tipoAgente: string;
}