import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { config } from './config';
@Injectable()
export class OnsCadastroUsuarioService {
  public _permiteNovoUsuario: boolean = false;

  set permiteNovoUsuario(value: boolean) {
    this._permiteNovoUsuario = value
  }

  get permiteNovoUsuario(): boolean {
    return this._permiteNovoUsuario;
  }

  constructor(private http: HttpClient) {
    this.tst()
  }


  salvarUsuario(): Observable<any> {
    return Observable.of({
      id: 0,
      login: "nilfton.jacintho@gmail.com",
      sid: "S-1-9-1089059503-1749794877-1963312245",
      nome: "teste-brq"
    });//this.http.post("",{});
  }

  salvarUsuarioErro(): Observable<any> {
    return Observable.of({
      "codigo": 400,
      "menssagem": "O campo Documento não possui numero de caracteres válidos 000.000.000-00 (cpf).\r\nO campo Documento não possui numero de caracteres válidos 000.000.000-00 (cpf)."
    });
    //this.http.post("",{});
  }

  carregarCondicoes(): Observable<any> {
    return this.http.get("");
  }

  carregarPerfis(): Observable<any> {
    return this.http.get(config.TST.URL_SINTEGRE + config.TST.URL_SINTEGRE_PROXYS.listaPerfilPublico)
  }

  verificaEmailOns(email: string): Observable<any> {
    return this.http.get(config.TST.URL_SINTEGRE + config.TST.URL_SINTEGRE_PROXYS.verificaEmail + email)
  }

  tst() {
    console.log('entrando em tst');
    this.http.get(config.TST.URL_SINTEGRE + config.TST.URL_SINTEGRE_PROXYS.listaPerfilPublico).subscribe((res) => {
      console.log(res);

    })
  }

}
